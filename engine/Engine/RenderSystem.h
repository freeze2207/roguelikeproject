///-------------------------------------------------------------------------------------------------
// file: RenderSystem.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include "SFML/Graphics.hpp"

class IRenderable;

/// <summary>
/// Defines the RenderSystem class
/// </summary>
class RenderSystem final
{
private:
	std::string name = "";
	int width = 1024;
	int height = 768;
	bool fullscreen = false;
	int m_layerSize = 1;
	std::string m_defaultFont = "../../../Engine/Engine/Vogue.ttf";
	std::string m_defaultTexture = "../../../Engine/Engine/default_texture.png";
	std::string m_requiredFontPath = "";
	std::string m_requiredTexturePath = "";
	std::list<IRenderable*> renderables;

	sf::RenderWindow* window = nullptr;

public:
	/// <summary>
	/// Adds a renderable to the list
	/// </summary>
	/// <param name="renderable"></param>
	void AddRenderable(IRenderable* renderable);

	/// <summary>
	/// Removes a renderable from the list
	/// </summary>
	/// <param name="renderable"></param>
	void RemoveRenderable(IRenderable* renderable);

	bool HasRenderWindow() { return (window != nullptr); }
	sf::RenderWindow& GetWindow() { return *window; }

private:
	/// <summary>
	/// Closes the window and sets it nullptr
	/// </summary>
	void CloseWindow();

	/// <summary>
	/// Opens a render window based off the parameters
	/// </summary>
	void Initialize();

	bool CopyFile(const char* _source, const char* _destination);

	/// <summary>
	/// Loads the RenderSettings
	/// </summary>
	/// <param name="document"></param>
	void Load(json::JSON& document);

	/// <summary>
	/// Updates the RenderSystem
	/// </summary>
	void Update(float total_time);

	DECLARE_SINGLETON(RenderSystem)

	friend class GameEngine;
	friend class InputManager;
	friend class GamePlay;
};

#endif //OBJECT_H