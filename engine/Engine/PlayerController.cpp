#include "Core.h"
#include "PlayerController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "EngineTime.h"


IMPLEMENT_DYNAMIC_CLASS(PlayerController)

void PlayerController::Initialize()
{
	inputManager = &InputManager::Instance();
}

void PlayerController::Start()
{
	
}

void PlayerController::Update()
{
	// Comment out original game engine move code

	/*sf::Vector2f movement = sf::Vector2f(0, 0);
	if (m_enable)
	{
		if (moveType == MovementType::SNAPPED)
		{
			if (inputManager->KeyPressed(sf::Keyboard::W))
			{
				movement.y -= speed;
			}
			if (inputManager->KeyPressed(sf::Keyboard::A))
			{
				movement.x -= speed;
			}
			if (inputManager->KeyPressed(sf::Keyboard::S))
			{
				movement.y += speed;
			}
			if (inputManager->KeyPressed(sf::Keyboard::D))
			{
				movement.x += speed;
			}
		}
		else if (moveType == MovementType::SMOOTH)
		{
			float deltaTime = EngineTime::Instance().GetDeltaTime();
			if (inputManager->KeyDown(sf::Keyboard::W))
			{
				movement.y -= speed * deltaTime;
			}
			if (inputManager->KeyDown(sf::Keyboard::A))
			{
				movement.x -= speed * deltaTime;
			}
			if (inputManager->KeyDown(sf::Keyboard::S))
			{
				movement.y += speed * deltaTime;
			}
			if (inputManager->KeyDown(sf::Keyboard::D))
			{
				movement.x += speed * deltaTime;
			}
		}

		game_object->SetPosition(game_object->GetPosition() + movement);
	}

	*/
}

void PlayerController::Load(json::JSON& component)
{
	if (component.hasKey("enable"))
	{
		m_enable = component["enable"].ToBool();
	}
	if (component.hasKey("speed"))
	{
		speed = component["speed"].ToFloat();
	}
	if (component.hasKey("movement_type"))
	{
		if (component["movement_type"].ToString() == "smooth")
		{
			moveType = MovementType::SMOOTH;
		}
		else if (component["movement_type"].ToString() == "snapped")
		{
			moveType = MovementType::SNAPPED;
		}
	}
	// More attributes for default player controller can be loaded here
}

