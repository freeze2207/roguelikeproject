///-------------------------------------------------------------------------------------------------
// file: FileSystem.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef FILESYSTEM_H
#define FILESYSTEM_H

/// <summary>
/// Defines the FileSystem class
/// </summary>
class FileSystem final
{
public:
	/// <summary>
	/// 
	/// </summary>
	/// <param name="file_name"></param>
	void Load(const std::string& file_name);

	DECLARE_SINGLETON(FileSystem)

private:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	friend class GameEngine;
};

#endif //FILESYSTEM_H
