#include "Core.h"
#include "EngineTime.h"

IMPLEMENT_SINGLETON(EngineTime)

void EngineTime::Initialize()
{
	time = std::chrono::system_clock::now();
	delta_time = std::chrono::duration<float>(0);
	total_time = std::chrono::duration<float>(0);
	current_frame = 0;
}

void EngineTime::Update()
{
	delta_time = std::chrono::system_clock::now() - time;
	time = std::chrono::system_clock::now();

	total_time += delta_time;
	current_frame++;
}
