#pragma once

#ifndef COLLIDABLE_H
#define COLLIDABLE_H

#include "SFML/Graphics.hpp"

class ICollidable
{
public:
	ICollidable();
	~ICollidable();

protected:
	const virtual sf::Shape& GetShape() = 0;

	virtual void OnCollisionEnter(ICollidable* collider);
	virtual void OnCollisionStay(ICollidable* collider);
	virtual void OnCollisionExit(ICollidable* collider);

	friend class CollisionEngine;
};
#endif
