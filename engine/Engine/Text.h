#pragma once
#ifndef TEXT_H
#define TEXT_H

#include "Component.h"
#include "IRenderable.h"
#include "SFML/Graphics.hpp"

class Text final : public Component, IRenderable
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Text, Component)

public:
	enum class Align
	{
		Left = 0,
		Center,
		Right
	};

	sf::FloatRect GetBounds() override;
	bool Contains(float _positionX, float _positionY) override;
	int GetLayer() override;
	void SetLayer(const int& _layer);

protected:
	~Text() override {}

	void Initialize() override;

	void Start() override;

	void Load(json::JSON& json_component) override;

	void Render(float total_time) override;

private:
	sf::Text		m_text;
	sf::Font		m_font;
	unsigned int	m_fontSize = 1;
	sf::Color		m_fontColor = sf::Color::Black;
	sf::Text::Style	m_fontStyle = sf::Text::Style::Regular;
	std::string		m_content;
	Text::Align		m_origin;
	int				m_layer = 0;
	bool			m_enable;

public: 

	void SetFont(const sf::Font& _font);
	void SetFontSize(const unsigned int& _fontSize);
	void SetFontColor(const sf::Color& _color);
	void SetStyle(const sf::Text::Style& _style);
	void SetText(const std::string& _text);
	void SetOrigin(const Text::Align& _origin);

	const sf::Font GetFont();
	const unsigned int GetFontSize();
	const sf::Color GetFontColor();
	const sf::Text::Style GetFontStyle();
	const std::string GetText();
	const Text::Align GetOrigin();

	void Enable() { m_enable = true; }
	void Disable() { m_enable = false; }
	bool GetStatus() override { return m_enable; }
};

#endif // !TEXT_H
