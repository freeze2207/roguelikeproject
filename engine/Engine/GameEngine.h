///-------------------------------------------------------------------------------------------------
// file: GameEngine.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef GAMEENGINE_H
#define GAMEENGINE_H

class IProjectEngine;

/// <summary>
/// Defines the GameEngine class
/// </summary>
class GameEngine final
{
public:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	/// <summary>
	/// 
	/// </summary>
	void SetProjectEngine(IProjectEngine* _project_engine) { project_engine = _project_engine; }

	DECLARE_SINGLETON(GameEngine)

private:
	IProjectEngine* project_engine = nullptr;
};

#endif //GAMEENGINE_H
