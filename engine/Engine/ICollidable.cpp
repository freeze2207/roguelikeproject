#include "Core.h"
#include "ICollidable.h"
#include "CollisionEngine.h"

ICollidable::ICollidable()
{
	CollisionEngine::Instance().AddCollidable(this);
}

ICollidable::~ICollidable()
{
	CollisionEngine::Instance().RemoveCollidable(this);
}

void ICollidable::OnCollisionEnter(ICollidable* collider)
{
	//std::cout << "CollisionEnter" << std::endl;
}

void ICollidable::OnCollisionStay(ICollidable* collider)
{
	//std::cout << "CollisionStay" << std::endl;
}

void ICollidable::OnCollisionExit(ICollidable* collider)
{
	//std::cout << "CollisionExit" << std::endl;
}
