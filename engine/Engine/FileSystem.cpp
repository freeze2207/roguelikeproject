///-------------------------------------------------------------------------------------------------
// file: FileSystem.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "FileSystem.h"

IMPLEMENT_SINGLETON(FileSystem)

void FileSystem::Initialize()
{
	std::cout << "File System initialized" << std::endl;
}

void FileSystem::Load(const std::string& file_name)
{
	// Load a file here
}

void FileSystem::Update()
{
}