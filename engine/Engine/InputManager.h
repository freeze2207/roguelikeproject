///-------------------------------------------------------------------------------------------------
// file: InputManger.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <list>
#include <map>
#include "SFML/Window.hpp"

static struct SubFunc
{
public:
	int priority;
	std::function<void()> func;

	SubFunc(int Priority, std::function<void()> Func) {
		priority = Priority;
		func = Func;
	}
};

typedef std::map<sf::Mouse::Button, std::list<SubFunc*>> MouseMap;
typedef std::map<unsigned int, std::list<SubFunc*>> JoystickMap;
typedef std::map<sf::Keyboard::Key, std::list<SubFunc*>> KeyMap;

/// <summary>
/// Defines the InputManager class
/// </summary>
class InputManager final
{
public:
	enum class InputState {
		Pressed,
		Down,
		Released
	};

	/// <summary>
	/// Toggle whether sfml constantly fires key press events when a key is held down
	/// </summary>
	/// <param name="toggle">True to enable the feature, false to turn it off</param>
	void ToggleKeyPressedRepeated(bool toggle);

private:
	/// <summary>
	/// Initialize the InputManager
	/// </summary>
	void Initialize();

	/// <summary>
	/// Updates the InputManager
	/// </summary>
	void Update();

	DECLARE_SINGLETON(InputManager)

	friend class GameEngine;

	/// <summary>
	/// Gets the address of the function the pointer points to
	/// </summary>
	/// <param name="func">The function pointer</param>
	/// <returns>The address of the function</returns>
	long FunctionPointerAddress(std::function<void()> func);

	/// <summary>
	/// Run the functions that are subscribed to given button
	/// </summary>
	/// <param name="subFuncs">List of functions to run</param>
	void RunSubscriptions(std::list<SubFunc*> subFuncs);


#pragma region Mouse
private:
	// List of pressed mouse buttons
	std::list<sf::Mouse::Button> pressedMouse;
	// List of released mouse buttons
	std::list<sf::Mouse::Button> releasedMouse;
	// Map of functions subscribed to mouse pressed
	MouseMap mouseSubscribePressed;
	// Map of functions subscribed to mouse down
	MouseMap mouseSubscribeDown;
	// Map of function subscribed to mouse released
	MouseMap mouseSubscribeReleased;

	/// <summary>
	/// Clear the button caches for mouse buttons
	/// </summary>
	void ClearMouseCaches();

public:
	/// <summary>
	/// Get whether a given mouse button was pressed this frame
	/// </summary>
	/// <param name="button">Mouse button to check</param>
	/// <returns>True if given mouse button was pressed this frame, false otherwise</returns>
	bool MouseButtonPressed(sf::Mouse::Button button);

	/// <summary>
	/// Get whether a given mouse button was released this frame
	/// </summary>
	/// <param name="button">Mouse button to check</param>
	/// <returns>True if given mouse button was released this frame, false otherwise</returns>
	bool MouseButtonReleased(sf::Mouse::Button button);

	/// <summary>
	/// Get whether a given mouse button is down this frame
	/// </summary>
	/// <param name="button">Mouse button to check</param>
	/// <returns>True if given mouse button is down this frame, false otherwise</returns>
	bool MouseButtonDown(sf::Mouse::Button button);

	/// <summary>
	/// Get the current position of the mouse
	/// </summary>
	/// <returns>The mouse window position</returns>
	sf::Vector2i GetMousePosition();

	/// <summary>
	/// Subscribe to a mouse event. Will call function as soon as event is recorded.
	/// </summary>
	/// <param name="state">Which state of the button to listen for.</param>
	/// <param name="button">Which button to listen for.</param>
	/// <param name="func_pointer">Function to run when event recorded.</param>
	/// <param name="priority">Priority of the function, 0 is default.</param>
	void SubscribeMouse(InputState state, sf::Mouse::Button button, std::function<void()> func_pointer, int priority = 0);

	/// <summary>
	/// Unsubscribe from a mouse event.
	/// </summary>
	/// <param name="state">State function was subscribed to.</param>
	/// <param name="button">Button function was subscribed to.</param>
	/// <param name="func_pointer">Function pointer.</param>
	void UnsubscribeMouse(InputState state, sf::Mouse::Button button, std::function<void()> func_pointer);

#pragma endregion Mouse


#pragma region Joystick
private:
	// Array of lists of pressed joystick buttons, max 8 joysticks
	std::list<unsigned int> pressedJoystick[8];
	//  Array of lists of released joystick buttons, max 8 joysticks
	std::list<unsigned int> releasedJoystick[8];
	// Array of maps of functions subscribed to joystick pressed, max 8 joysticks
	JoystickMap joystickSubscribePressed[8];
	// Array of maps of functions subscribed to joystick down, max 8 joysticks
	JoystickMap joystickSubscribeDown[8];
	// Array of maps of function subscribed to joystick released, max 8 joysticks
	JoystickMap joystickSubscribeReleased[8];

	/// <summary>
	/// Clear the button caches for joystick buttons
	/// </summary>
	void ClearJoystickCaches();

public:
	/// <summary>
	/// Get whether a given joystick button was pressed this frame
	/// </summary>
	/// <param name="id">Id of Joystick</param>
	/// <param name="button">Joystick button to check</param>
	/// <returns>True if given joystick button was pressed this frame, false otherwise</returns>
	bool JoystickButtonPressed(int id, unsigned int button);

	/// <summary>
	/// Get whether a given joystick button was released this frame
	/// </summary>
	/// <param name="id">Id of Joystick</param>
	/// <param name="button">Joystick button to check</param>
	/// <returns>True if given joystick button was released this frame, false otherwise</returns>
	bool JoystickButtonReleased(int id, unsigned int button);

	/// <summary>
	/// Get whether a given joystick button is down this frame
	/// </summary>
	/// <param name="id">Id of Joystick</param>
	/// <param name="button">Joystick button to check</param>
	/// <returns>True if given joystick button is down this frame, false otherwise</returns>
	bool JoystickButtonDown(int id, unsigned int button);

	/// <summary>
	/// Subscribe to a joystick event. Will call function as soon as event is recorded.
	/// </summary>
	/// <param name="id">Id of Joystick</param>
	/// <param name="state">Which state of the button to listen for.</param>
	/// <param name="button">Which button to listen for.</param>
	/// <param name="func_pointer">Function to run when event recorded.</param>
	/// <param name="priority">Priority of the function, 0 is default.</param>
	void SubscribeJoystick(int id, InputState state, unsigned int button, std::function<void()> func_pointer, int priority = 0);

	/// <summary>
	/// Unsubscribe from a joystick event.
	/// </summary>
	/// <param name="id">Id of Joystick</param>
	/// <param name="state">State function was subscribed to.</param>
	/// <param name="button">Button function was subscribed to.</param>
	/// <param name="func_pointer">Function pointer.</param>
	void UnsubscribeJoystick(int id, InputState state, unsigned int button, std::function<void()> func_pointer);

	/// <summary>
	/// Get the axis position of joystick on given joystick
	/// </summary>
	/// <param name="id">id of joystick (0-7)</param>
	/// <param name="axis">Joystick axis to check</param>
	/// <returns>Axis value of specified joystick</returns>
	float JoystickAxisPosition(int id, sf::Joystick::Axis axis);

	/// <summary>
	/// Get the number of joysticks detected by SFML
	/// </summary>
	/// <returns>The number of detected joysticks</returns>
	int JoysticksDetected();

#pragma endregion Joystick

#pragma region Keyboard
private:
	// List of pressed key buttons
	std::list<sf::Keyboard::Key> pressedKey;
	// List of released key buttons
	std::list<sf::Keyboard::Key> releasedKey;
	// map of functions subscribed to key pressed
	KeyMap keySubscribePressed;
	// map of functions subscribed to key down
	KeyMap keySubscribeDown;
	// map of functions subscribed to key released
	KeyMap keySubscribeReleased;

	/// <summary>
	/// Clears the key caches for keyboard keys
	/// </summary>
	void ClearKeyCaches();

public:
	/// <summary>
	/// Get whether a given key was pressed this frame
	/// </summary>
	/// <param name="key">Keyboard key to check</param>
	/// <returns>True if given keyboard key was pressed this frame, otherwise false</returns>
	bool KeyPressed(sf::Keyboard::Key key);

	/// <summary>
	/// Get whether a given key was released this frame
	/// </summary>
	/// <param name="key">Keyboard key to check</param>
	/// <returns>True if given keyboard key was released this frame, otherwise false</returns>
	bool KeyReleased(sf::Keyboard::Key key);

	/// <summary>
	/// Get whether a given key was down this frame
	/// </summary>
	/// <param name="key">Keyboard key to check</param>
	/// <returns></returns>
	bool KeyDown(sf::Keyboard::Key key);

	/// <summary>
	/// Subscribe to a key event. Will call function as soon as event is recorded.
	/// </summary>
	/// <param name="state">Which state of the key to listen for</param>
	/// <param name="key">Which key to listen for</param>
	/// <param name="func_pointer">Function to run when event recorded</param>
	/// <param name="priority">Priority of the function, 0 is default</param>
	void SubscribeKey(InputState state, sf::Keyboard::Key key, std::function<void()> func_pointer, int priority = 0);

	/// <summary>
	/// Unsubscribe from a key event
	/// </summary>
	/// <param name="state">State function was subscribed to</param>
	/// <param name="key">Key function was subscribed to</param>
	/// <param name="func_pointer">Function pointer</param>
	void UnsubscribeKey(InputState state, sf::Keyboard::Key key, std::function<void()> func_pointer);

#pragma endregion Keyboard
};

#endif //INPUTMANAGER_H