#include "Core.h"
#include "ObjectPool.h"

//template<class T>
//ObjectPool<T>::ObjectPool()
//{
//}

template<class T>
inline ObjectPool<T>::ObjectPool(int _size)
	:poolSize(_size)
{
	for (int i = 0; i < poolSize; i++)
	{
		objects.push_back(new T());
	}
}

template<class T>
ObjectPool<T>::~ObjectPool()
{
	if (objects.size == poolSize)
	{
		for (T* object : objects)
		{
			delete object;
		}
	}
	else
	{
		std::cout << "Not all objects back to pool" << std::endl;
		assert(0);
	}
}

template<class T>
T* ObjectPool<T>::GetPoolObject()
{
	if (objects.size > 0)
	{
		return objects.pop_back();
	}
	else
	{
		std::cout << "Empty pool" << std::endl;
		return nullptr;
	}
}

template<class T>
void ObjectPool<T>::Retrieve(T* _object)
{
	if (objects.size != poolSize)
	{
		objects.push_back(_object);
	}
	else
	{
		std::cout << "Retrieving error object" << std::endl;
	}
}
