#pragma once
#ifndef OBJECTPOOL_H
#define OBJECTPOOL_H
#include "GameObject.h"

template <class T>
class ObjectPool
{
private:
	std::vector<T> objects;
	int poolSize = 0;

public:
	//ObjectPool();
	ObjectPool(int _size);
	~ObjectPool();

	T* GetPoolObject();
	void Retrieve(T* _object);
};

#endif // !OBJECTPOOL_H

