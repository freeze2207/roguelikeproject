#include "Core.h"
#include "Collider.h"
#include "GameObject.h"

IMPLEMENT_ABSTRACT_CLASS(Collider)
Collider::Collider()
{

}

Collider::~Collider()
{

}

void Collider::OnCollisionEnter(ICollidable* collider)
{
	ICollidable::OnCollisionEnter(collider);
	for (auto func : collisionEnterCallbacks)
	{
		func(collider);
	}
}

void Collider::OnCollisionStay(ICollidable* collider)
{
	ICollidable::OnCollisionStay(collider);
	for (auto func : collisionStayCallbacks)
	{
		func(collider);
	}
}

void Collider::OnCollisionExit(ICollidable* collider)
{
	ICollidable::OnCollisionExit(collider);
	//std::cout << "EXIT: " << game_object->GetName() << std::endl;
	for (auto func : collisionExitCallbacks)
	{
		func(collider);
	}
}

void Collider::RegisterEnterCallback(std::function<void(ICollidable*)> func)
{
	collisionEnterCallbacks.push_back(func);
}

void Collider::RegisterStayCallback(std::function<void(ICollidable*)> func)
{
	collisionStayCallbacks.push_back(func);
}

void Collider::RegisterExitCallback(std::function<void(ICollidable*)> func)
{
	collisionExitCallbacks.push_back(func);
}

