///-------------------------------------------------------------------------------------------------
// file: GameObjectManager.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "Component.h"

IMPLEMENT_SINGLETON(GameObjectManager)

GameObjectManager::~GameObjectManager()
{
	for (auto game_object : game_objects)
	{
		delete game_object;
	}
	game_objects.clear();
}

void GameObjectManager::Initialize()
{
	std::cout << "GameObjectManager initialized" << std::endl;
}

void GameObjectManager::Load(json::JSON& document)
{
	if (document.hasKey("GameObjects") == false) return;

	json::JSON json_game_objects = document["GameObjects"];
	for (auto& json_game_object : json_game_objects.ArrayRange())
	{
		GameObject* game_object = new GameObject();
		game_object->Load(json_game_object);
		game_object->Initialize();
		AddGameObject(game_object);
	}

	/// Post Init
	//for (auto game_object : game_objects)
	//{
	//	game_object->Start();
	//}
}

void GameObjectManager::Unload()
{
	for (auto game_object : game_objects)
	{
		delete game_object;
	}
	game_objects.clear();
	std::cout << "Deleted game objects" << std::endl;
}

GameObject* GameObjectManager::CreateGameObject(const std::vector<std::string>& component_list)
{
	GameObject* go = new GameObject();
	go->AddComponents(component_list);
	go->Initialize();

	return go;
}

GameObject* GameObjectManager::CreateGameObject(json::JSON& _gameObject)
{
	GameObject* game_object = new GameObject();
	game_object->Load(_gameObject);
	game_object->Initialize();
	AddGameObject(game_object);

	return game_object;
}

GameObject* GameObjectManager::FindGameObjectByName(std::string object_name)
{
	for (auto game_object : game_objects)
	{
		if (game_object->GetName() == object_name)
		{
			return game_object;
		}
	}

	return nullptr;
}

std::list<GameObject*> GameObjectManager::FindAllGameObjectsWithComponent(const std::string& comp_type)
{
	std::list<GameObject*> found_game_objects;
	for ( auto game_object : game_objects)
	{
		if (game_object->FindComponentByType(comp_type) != nullptr)
		{
			found_game_objects.push_back(game_object);
		}
		
	}
	return found_game_objects;
}

void GameObjectManager::Update()
{
	for (auto game_object : game_objects)
	{
		game_object->Update();
	}

	for (auto game_object : game_objects_to_remove)
	{
		game_objects.remove(game_object);
		delete game_object;
	}
	game_objects_to_remove.clear();
}

void GameObjectManager::AddGameObject(GameObject* game_object)
{
	// There could be some code here
	game_objects.push_back(game_object);
}

void GameObjectManager::RemoveGameObject(GameObject* game_object)
{
	game_objects_to_remove.push_back(game_object);
}