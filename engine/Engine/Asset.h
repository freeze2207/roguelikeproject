///-------------------------------------------------------------------------------------------------
// file: Asset.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef ASSET_H
#define ASSET_H

/// <summary>
/// Base Asset class
/// </summary>
class Asset
{
public:
	virtual ~Asset() = 0;
};

#endif // !ASSET_H

