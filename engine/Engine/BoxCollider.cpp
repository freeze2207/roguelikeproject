#include "Core.h"
#include "BoxCollider.h"
#include "GameObject.h"

IMPLEMENT_DYNAMIC_CLASS(BoxCollider)

BoxCollider::BoxCollider()
{
}

BoxCollider::~BoxCollider()
{
	delete collisionShape;
}

const sf::Shape& BoxCollider::GetShape()
{
	return *collisionShape;
}

void BoxCollider::SetShape(const float& _x, const float& _y)
{
	collisionShape = new sf::RectangleShape(sf::Vector2f(_x, _y));
}

void BoxCollider::Initialize()
{

}

void BoxCollider::Start()
{
	
}

void BoxCollider::Update()
{
	collisionShape->setPosition(game_object->GetPosition());
}

void BoxCollider::Load(json::JSON& component)
{
	if (component.hasKey("rectangle_size"))
	{
		json::JSON size = component["rectangle_size"];
		if (size.hasKey("x") & size.hasKey("y"))
		{
			float x = size["x"].ToFloat(); float y = size["y"].ToFloat();
			collisionShape = new sf::RectangleShape(sf::Vector2f(x, y));
			collisionShape->setOrigin(collisionShape->getSize().x * 0.5, collisionShape->getSize().y * 0.5);
		}
	}
}
