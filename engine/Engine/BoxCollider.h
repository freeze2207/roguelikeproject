#pragma once
#ifndef BOXCOLLIDER_H
#define BOXCOLLIDER_H


#include "Collider.h"


class BoxCollider : public Collider
{
	DECLARE_DYNAMIC_DERIVED_CLASS(BoxCollider, Collider)

private:
	sf::RectangleShape* collisionShape = nullptr;

public:

	BoxCollider();
	~BoxCollider();
	virtual const sf::Shape& GetShape();

	void SetShape(const float& _x, const float& _y);
	
protected:

	/// <summary>
	/// Initialize the BoxCollider
	/// </summary>
	void Initialize() override;

	void Start() override;

	/// <summary>
	/// Update the BoxCollider
	/// </summary>
	void Update() override;

	void Load(json::JSON& component) override;

};

#endif // !BOXCOLLIDER_H