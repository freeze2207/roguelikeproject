///-------------------------------------------------------------------------------------------------
// file: RenderSystem.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "RenderSystem.h"
#include "IRenderable.h"
#include <fstream>

IMPLEMENT_SINGLETON(RenderSystem)

void RenderSystem::CloseWindow()
{
	window->close();
	delete window;
	window = nullptr;
}

void RenderSystem::Initialize()
{
	std::cout << "RenderSystem initialized" << std::endl;

	window = new sf::RenderWindow(sf::VideoMode(width, height), name);
	window->setFramerateLimit(60);

	std::ifstream check_FontFile(m_requiredFontPath.c_str());
	if (!check_FontFile.good())
	{
		//copy 
		CopyFile(m_defaultFont.c_str(), m_requiredFontPath.c_str());
	}
	std::ifstream check_TextureFile(m_requiredTexturePath.c_str());
	if (!check_TextureFile.good())
	{
		//copy
		CopyFile(m_defaultTexture.c_str(), m_requiredTexturePath.c_str());
	}
}

bool RenderSystem::CopyFile(const char* _source, const char* _destination)
{
	std::ifstream src(_source, std::ios::binary);
	std::ofstream dest(_destination, std::ios::binary);
	dest << src.rdbuf();
	return src && dest;
}

void RenderSystem::Load(json::JSON& document)
{
	if (document.hasKey("RenderSystem"))
	{
		json::JSON render_system = document["RenderSystem"];

		if (render_system.hasKey("Name"))
		{
			name = render_system["Name"].ToString();
		}
		if (render_system.hasKey("width"))
		{
			width = render_system["width"].ToInt();
		}
		if (render_system.hasKey("height"))
		{
			height = render_system["height"].ToInt();
		}
		if (render_system.hasKey("fullscreen"))
		{
			fullscreen = render_system["fullscreen"].ToBool();
		}
		if (render_system.hasKey("layerSize"))
		{
			m_layerSize = render_system["layerSize"].ToInt();
		}

		// Required fields for Game settings
		ASSERT(render_system.hasKey("defaultFontPath"), "Default font path node missing");
		ASSERT(render_system.hasKey("defaultTexturePath"), "Default texture path node missing");

		m_requiredFontPath = render_system["defaultFontPath"].ToString();
		m_requiredTexturePath = render_system["defaultTexturePath"].ToString();

	}
}

void RenderSystem::Update(float total_time)
{
	if (window != nullptr)
	{
		window->clear();

		// a simple layer render
		int layer = 0;
		while (layer < m_layerSize)
		{
			for (auto renderable : renderables)
			{
				if (renderable->GetLayer() == layer && renderable->GetStatus() != false)
				{
					renderable->Render(total_time);
				}
			}
			layer++;
		}

		window->display();
		layer = 0;
	}
}

void RenderSystem::AddRenderable(IRenderable* renderable)
{
	renderables.push_back(renderable);
}

void RenderSystem::RemoveRenderable(IRenderable* renderable)
{
	renderables.remove(renderable);
}
