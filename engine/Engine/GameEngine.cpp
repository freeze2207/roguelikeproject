///-------------------------------------------------------------------------------------------------
// file: GameEngine.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "GameEngine.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"
#include "EngineTime.h"
#include "IProjectEngine.h"
#include "CollisionEngine.h"

IMPLEMENT_SINGLETON(GameEngine)

extern void RegisterEngineClasses();

void GameEngine::Initialize()
{
	RegisterEngineClasses();

	FileSystem::Instance().Initialize();
	
	AssetManager::Instance().Initialize();
	GameObjectManager::Instance().Initialize();
	CollisionEngine::Instance().Initialize();

	std::ifstream game_settings_stream("../Assets/GameSettings.json");
	std::string game_settings_str((std::istreambuf_iterator<char>(game_settings_stream)), std::istreambuf_iterator<char>());
	json::JSON game_settings_document = json::JSON::Load(game_settings_str);

	RenderSystem::Instance().Load(game_settings_document);
	RenderSystem::Instance().Initialize();

	assert(game_settings_document.hasKey("GameEngine"));
	json::JSON game_engine = game_settings_document["GameEngine"];

	assert(game_engine.hasKey("DefaultFile"));
	std::string default_file = game_engine["DefaultFile"].ToString();

	std::ifstream default_file_stream(default_file.c_str());
	std::string default_file_str((std::istreambuf_iterator<char>(default_file_stream)), std::istreambuf_iterator<char>());
	json::JSON default_file_document = json::JSON::Load(default_file_str);

	GameObjectManager::Instance().Load(default_file_document);

	InputManager::Instance().Initialize();
	/*if (project_engine != nullptr)
	{
		project_engine->Update();
	}*/
}

void GameEngine::Update()
{
	EngineTime::Instance().Initialize();

	while (RenderSystem::Instance().HasRenderWindow())
	{
		EngineTime::Instance().Update();
		FileSystem::Instance().Update();
		AssetManager::Instance().Update();
		InputManager::Instance().Update();
		GameObjectManager::Instance().Update();
		CollisionEngine::Instance().Update();

		if (project_engine != nullptr)
		{
			project_engine->Update();
		}

		RenderSystem::Instance().Update(EngineTime::Instance().GetTotalTime());
	}
}