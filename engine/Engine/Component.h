///-------------------------------------------------------------------------------------------------
// file: Component.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef COMPONENT_H
#define COMPONENT_H

#include "Object.h"

class GameObject;

/// <summary>
/// Defines the Component class
/// </summary>
class Component : public Object
{
	DECLARE_ABSTRACT_DERIVED_CLASS(Component, Object)

public:
	
	GameObject* game_object;
	~Component() = 0;

protected:
	/// <summary>
	/// Initialize the component
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// Load component data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;

	/// <summary>
	/// Updates the Component
	/// </summary>
	virtual void Update();

	/// <summary>
	/// Starts the component. This will be called once all components are loaded
	/// in the GameObject. All components should be initialized before this is called.
	/// </summary>
	virtual void Start();

	friend class GameObject;
	friend class GameObjectManager;
};

#endif //COMPONENT_H