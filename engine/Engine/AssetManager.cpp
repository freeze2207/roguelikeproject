///-------------------------------------------------------------------------------------------------
// file: AssetManager.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "AssetManager.h"
#include "Asset.h"

IMPLEMENT_SINGLETON(AssetManager)

void AssetManager::Initialize()
{
	std::cout << "Asset Manager initialized" << std::endl;
}

void AssetManager::Update()
{
}

void AssetManager::AddAsset(Asset* asset)
{
	assets.push_back(asset);
}

void AssetManager::RemoveAsset(Asset* asset)
{
	assets.remove(asset);
}
