#include "Core.h"
#include "CollisionEngine.h"

IMPLEMENT_SINGLETON(CollisionEngine)

void CollisionEngine::AddCollidable(ICollidable* collidable)
{
	collidables.push_back(collidable);
}

void CollisionEngine::RemoveCollidable(ICollidable* collidable)
{
	collidables.remove(collidable);
}

void CollisionEngine::AddCollision(Collision* collision)
{
	for (auto col : activeCollisions)
	{
		if (col->equals(*collision))
		{
			delete collision;
			return;
		}
	}
	collision->collider1->OnCollisionEnter(collision->collider2);
	collision->collider2->OnCollisionEnter(collision->collider1);
	activeCollisions.push_back(collision);
}

void CollisionEngine::ParseActiveCollisions()
{
	//LOOP THROUGH ALL COLLISIONS
	for (auto collision : activeCollisions)
	{
		ICollidable* col1 = collision->collider1;
		ICollidable* col2 = collision->collider2;
		if (IsColliding(col1, col2))
		{
			col1->OnCollisionStay(col2);
			col2->OnCollisionStay(col1);
		}
		else
		{
			collisionsToRemove.push_back(collision);
		}
	}
}

void CollisionEngine::ParseRemovedCollisions()
{
	for (auto collision : collisionsToRemove)
	{
		ICollidable* col1 = collision->collider1;
		ICollidable* col2 = collision->collider2;
		col1->OnCollisionExit(col2);
		col2->OnCollisionExit(col1);
		activeCollisions.remove(collision);
		delete collision;
	}
	collisionsToRemove.clear();
}

bool CollisionEngine::IsColliding(ICollidable* col1, ICollidable* col2)
{
	return col1->GetShape().getGlobalBounds().intersects(col2->GetShape().getGlobalBounds());
}

void CollisionEngine::Update()
{
	for (auto& col1 : collidables)
	{
		for (auto& col2 : collidables)
		{
			if (col1 == col2) continue;

			if (IsColliding(col1, col2))
			{
				AddCollision(new Collision(col1, col2));
			}
		}
	}
	ParseActiveCollisions();
	ParseRemovedCollisions();
}

void CollisionEngine::Initialize()
{

}

