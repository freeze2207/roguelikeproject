///-------------------------------------------------------------------------------------------------
// file: registerClasses.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"

#include "Object.h"
#include "Component.h"
#include "Sprite.h"
#include "GameObject.h"
#include "Text.h"
#include "PlayerController.h"
#include "Collider.h"
#include "BoxCollider.h"

/// <summary>
/// When adding a new class to the system you must do the following.
/// 1. DECLARE
/// 2. IMPLEMENT
/// 3. REGISTER - which is done here
/// </summary>
void RegisterEngineClasses()
{
	REGISTER_ABSTRACT_CLASS(Object);
	REGISTER_ABSTRACT_CLASS(Component);
	REGISTER_ABSTRACT_CLASS(Collider);
	REGISTER_DYNAMIC_CLASS(Sprite);
	REGISTER_DYNAMIC_CLASS(GameObject);
	REGISTER_DYNAMIC_CLASS(Text);
	REGISTER_DYNAMIC_CLASS(PlayerController);
	REGISTER_DYNAMIC_CLASS(BoxCollider);

}