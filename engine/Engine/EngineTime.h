#pragma once
#ifndef ENGINETIME_H
#define ENGINETIME_H

class EngineTime
{
private:
	std::chrono::time_point<std::chrono::system_clock> time;
	std::chrono::duration<float> delta_time;
	std::chrono::duration<float> total_time;
	int current_frame = 0;

public:
	float GetDeltaTime() { return delta_time.count(); }
	float GetTotalTime() { return total_time.count(); }
	int GetCurrentFrame() { return current_frame; }

private:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	DECLARE_SINGLETON(EngineTime)

	friend class GameEngine;
};

#endif // !ENGINETIME_H
