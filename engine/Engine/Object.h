///-------------------------------------------------------------------------------------------------
// file: Object.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef OBJECT_H
#define OBJECT_H

/// <summary>
/// Base class to all
/// </summary>
class Object
{
	DECLARE_ABSTRACT_BASE_CLASS(Object)

private:
	bool initialized = false;
	STRCODE uid = -1;
	std::string guid;

public:
	/// <summary>
	/// 
	/// </summary>
	Object();

	/// <summary>
	/// 
	/// </summary>
	virtual ~Object() = 0;

	/// <summary>
	/// 
	/// </summary>
	/// <param name="json_component"></param>
	virtual void Load(json::JSON& json_component);

	inline bool IsInitialized() { return initialized; }
	inline int GetId() { return uid; }
	inline const std::string& GetGUID() { return guid; }

protected:
	/// <summary>
	/// 
	/// </summary>
	virtual void Initialize();
};

#endif // !OBJECT_H