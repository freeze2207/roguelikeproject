#pragma once
#ifndef COLLISIONENGINE_H
#define COLLISIONENGINE_H


#include "ICollidable.h"


class CollisionEngine final
{
	struct Collision
	{
		ICollidable* collider1;
		ICollidable* collider2;

		Collision(ICollidable* _col1, ICollidable* _col2)
		{
			collider1 = _col1;
			collider2 = _col2;
		}
		bool equals(Collision& other)
		{
			if (collider1 == other.collider1 && collider2 == other.collider2) return true;
			if (collider1 == other.collider2 && collider2 == other.collider1) return true;
			else { return false; }
		}
	};

private:

	//List of all collidables
	std::list<ICollidable*> collidables;

	//List of active collisions
	std::list<Collision*> activeCollisions;
	std::list<Collision*> collisionsToRemove;

	void AddCollidable(ICollidable* collidable);
	void RemoveCollidable(ICollidable* collidable);

	void AddCollision(Collision* collision);
	void ParseActiveCollisions();
	void ParseRemovedCollisions();

	bool IsColliding(ICollidable* col1, ICollidable* col2);

protected:
	void Update();
	void Initialize();

	DECLARE_SINGLETON(CollisionEngine)
	friend class GameEngine;
	friend class ICollidable;
};

#endif // !COLLISIONENGINE_H