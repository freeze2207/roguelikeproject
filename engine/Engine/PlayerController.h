#pragma once
#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include "Component.h"

class InputManager;

/// <summary>
/// Default player controller with basic WASD support
/// </summary>
class PlayerController : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(PlayerController, Component)

	~PlayerController() override {}

	//Pointer to the input system instance
	InputManager* inputManager = nullptr;

	//The speed of the object, default is 32
	float speed = 32;

	enum class MovementType {
		SMOOTH,
		SNAPPED
	};

	MovementType moveType = MovementType::SMOOTH;

protected:

	/// <summary>
	/// Initialize the PlayerController
	/// </summary>
	void Initialize() override;

	void Start() override;

	/// <summary>
	/// Update the PlayerController
	/// </summary>
	void Update() override;

	void Load(json::JSON& component) override;

	bool m_enable = false;

public:

	void Enable() { m_enable = true; }
	void Disable() { m_enable = false; }

};
#endif // !PLAYERCONTROLLER_H


