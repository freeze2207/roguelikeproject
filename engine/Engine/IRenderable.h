///-------------------------------------------------------------------------------------------------
// file: IRenderable.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef IRENDERABLE_H
#define IRENDERABLE_H

#include "SFML/Graphics.hpp"

/// <summary>
/// Renderable aspect
/// </summary>
class IRenderable
{
protected:
	/// <summary>
	/// 
	/// </summary>
	IRenderable();

	/// <summary>
	/// 
	/// </summary>
	~IRenderable();

	/// <summary>
	/// Render method, must override to draw
	/// </summary>
	virtual void Render(float total_time) = 0;

	virtual int GetLayer() = 0;

	virtual sf::FloatRect GetBounds() = 0;

	virtual bool Contains(float _positionX, float _positionY) = 0;

	virtual bool GetStatus() = 0;

	friend class RenderSystem;
};

#endif // !IRENDERABLE_H
