///-------------------------------------------------------------------------------------------------
// file: Component.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "Component.h"

IMPLEMENT_ABSTRACT_CLASS(Component)

Component::~Component()
{
}

void Component::Initialize()
{
	Object::Initialize();
}

void Component::Load(json::JSON& json_component)
{
	Object::Load(json_component);
}

void Component::Update()
{
}

void Component::Start()
{
}
