///-------------------------------------------------------------------------------------------------
// file: Sprite.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "Sprite.h"
#include "RenderSystem.h"
#include "GameObject.h"
#include "EngineTime.h"

IMPLEMENT_DYNAMIC_CLASS(Sprite)

void Sprite::Load(json::JSON& json_component)
{
	Component::Load(json_component);

	//ASSERT(json_component.hasKey("uid"), "Json node missing");
	ASSERT(json_component.hasKey("texture_filename"), "Json node missing");
	texture_filename = json_component["texture_filename"].ToString();
	if (json_component.hasKey("layer"))
	{
		m_layer = json_component["layer"].ToInt();
	}
	if (json_component.hasKey("enable"))
	{
		m_enable = json_component["enable"].ToBool();
	}
	if (json_component.hasKey("startPosition"))
	{
		rectTarget.left = json_component["startPosition"]["x"].ToInt();
		rectTarget.top = json_component["startPosition"]["y"].ToInt();
	}
	if (json_component.hasKey("endPosition"))
	{
		isAnimated = true;
		m_animationStartPosition.x = rectTarget.left;
		m_animationStartPosition.y = rectTarget.top;
		m_animationEndPosition.x = json_component["endPosition"]["x"].ToInt();
		m_animationEndPosition.y = json_component["endPosition"]["y"].ToInt();
	}
	if (json_component.hasKey("width"))
	{
		m_offset = json_component["width"].ToInt();
		rectTarget.width = m_offset;
	}
	if (json_component.hasKey("height"))
	{
		rectTarget.height = json_component["height"].ToInt();
	}
	if (json_component.hasKey("interval"))
	{
		m_interval = json_component["interval"].ToFloat();
	}
	

}

void Sprite::Initialize()
{	
	
}

void Sprite::Start()
{
	std::ifstream check_file(texture_filename.c_str());
	if (check_file.good())
	{
		texture.loadFromFile(texture_filename);
	}
	else
	{
		ASSERT(texture.loadFromFile("../Assets/Textures/default_texture.png"), "Failed to load default texture");
		isAnimated = false;
	}
	textureSize = texture.getSize();
	m_time = EngineTime::Instance().GetTotalTime();

	if (isAnimated)
	{
		SetupAnimatedSprite(texture_filename);
	}
	else if (rectTarget.width != 0 && rectTarget.height != 0)
	{
		SetupStaticSprite(texture_filename, true);
	}
	else
	{
		SetupStaticSprite(texture_filename);
	}
}

void Sprite::Render(float total_time)
{
	// Check texture in render
	std::ifstream check_file(texture_filename.c_str());
	if (!check_file.good())
	{
		ASSERT(texture.loadFromFile("../Assets/Textures/default_texture.png"), "Failed to load default texture");
		sprite.setTexture(texture);
	}

	sprite.setPosition(game_object->GetPosition());
	if (isAnimated)
	{
		UpdateAnimation();
	}
	
	RenderSystem::Instance().GetWindow().draw(sprite);
}

void Sprite::UpdateAnimation()
{	
	// Loop through till find end Position	
	if (EngineTime::Instance().GetTotalTime() - m_time > m_interval) {
		if (rectTarget.left + m_offset == m_animationEndPosition.x && rectTarget.top + rectTarget.height == m_animationEndPosition.y)
		{
			rectTarget.left = m_animationStartPosition.x;
			rectTarget.top = m_animationStartPosition.y;
		}
		else
		{
			if (rectTarget.left + m_offset == textureSize.x)
			{
				rectTarget.left = 0;
				rectTarget.top += rectTarget.height;
			}
			else
			{
				rectTarget.left += m_offset;
			}
		}
		sprite.setTextureRect(rectTarget);
		m_time = EngineTime::Instance().GetTotalTime();
	}

}

int Sprite::GetLayer()
{
	return this->m_layer;
}

void Sprite::SetLayer(const int& _layer)
{
	this->m_layer = _layer;
}

void Sprite::SetupStaticSprite(std::string& _textureFileName)
{
	ASSERT(texture.loadFromFile(_textureFileName), "Failed to load texture");
	this->sprite.setTexture(texture);
}

// Read parameter from json version
void Sprite::SetupStaticSprite(std::string& _textureFileName, bool isSliced)
{
	ASSERT(texture.loadFromFile(_textureFileName, rectTarget), "Failed to load texture");
	this->sprite.setTexture(texture);
}

// Setting up sliced sprite parameters external passing version
void Sprite::SetupStaticSprite(std::string& _textureFileName, sf::Vector2u _startPosition, int _offset, int _height)
{
	sf::IntRect rect(_startPosition.x, _startPosition.y, _offset, _height);	
	ASSERT(texture.loadFromFile(_textureFileName, rect), "Failed to load texture");
	this->sprite.setTexture(texture);
}

// Read parameter from json version
void Sprite::SetupAnimatedSprite(std::string& _textureFileName)
{
	ASSERT(texture.loadFromFile(_textureFileName), "Failed to load texture");
	sprite = sf::Sprite(texture, rectTarget);
}

// Setting up Animation parameters external passing version
void Sprite::SetupAnimatedSprite(std::string& _textureFileName, sf::Vector2u _startPosition, sf::Vector2u _endPosition, int _offset, int _height, float _interval)
{
	ASSERT(texture.loadFromFile(_textureFileName), "Failed to load texture");
	m_animationStartPosition = _startPosition;
	m_animationEndPosition = _endPosition;
	m_offset = _offset;
	rectTarget.width = _offset;
	rectTarget.height = _height;
	rectTarget.left = _startPosition.x;
	rectTarget.top = _startPosition.y;
	m_interval = _interval;
	sprite = sf::Sprite(texture, rectTarget);
}

sf::FloatRect Sprite::GetBounds()
{
	return sprite.getGlobalBounds();
}

bool Sprite::Contains(float _positionX, float _positionY)
{	
	return m_enable ? sprite.getGlobalBounds().contains(sf::Vector2f(_positionX, _positionY)) : false;
}

sf::Vector2f Sprite::GetScale() const
{
	return this->m_scale;
}

void Sprite::SetScale(const sf::Vector2f& _scale)
{
	this->m_scale = _scale;
}

void Sprite::SetScale(const float& _scaleX, const float& _scaleY)
{
	this->m_scale = sf::Vector2f(_scaleX, _scaleY);
}

bool Sprite::IsAnimation()
{
	return this->isAnimated;
}

void Sprite::SetInterval(float _interval)
{
	this->m_interval = _interval;
}

const sf::Vector2f& Sprite::GetOrigin()
{
	return this->sprite.getOrigin();
}

void Sprite::SetOrigin(Sprite::Align _align)
{
	switch (_align)
	{
	case Align::TopLeft:
		sprite.setOrigin(0, 0);
		break;
	case Align::TopRight:
		sprite.setOrigin(texture.getSize().x, 0);
		break;
	case Align::Center:
		sprite.setOrigin(texture.getSize().x * 0.5f, texture.getSize().y * 0.5f);
		break;
	case Align::BottomLeft:
		sprite.setOrigin(0, texture.getSize().y);
		break;
	case Align::BottomRight:
		sprite.setOrigin(texture.getSize().x, texture.getSize().y);
		break;
	default:
		std::cout << "Wrong Align type" << std::endl;
		break;
	}
}

void Sprite::SetOrigin(sf::Vector2f& _origin)
{
	sprite.setOrigin(_origin.x, _origin.y);
}

