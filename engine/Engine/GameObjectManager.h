///-------------------------------------------------------------------------------------------------
// file: GameObjectManager.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include <list>
#include "json.hpp"

class GameObject;

/// <summary>
/// Defines the GameObjectManager class
/// </summary>
class GameObjectManager final
{
private:
	std::list<GameObject*> game_objects;
	std::list<GameObject*> game_objects_to_remove;

public:
	/// <summary>
	/// Loads all game objects avaialble in the document
	/// </summary>
	/// <param name="document"></param>
	void Load(json::JSON& document);

	// Unloads all game objects.
	void Unload();

	/// <summary>
	/// Add a GameObject
	/// </summary>
	void AddGameObject(GameObject* game_object);

	/// <summary>
	/// Tags a game object to be removed at the end of update
	/// </summary>
	/// <param name="game_object"></param>
	void RemoveGameObject(GameObject* game_object);

	/// <summary>
	/// Create a game object with a list of components.
	/// </summary>
	/// <param name="component_list"></param>
	/// <returns></returns>
	GameObject* CreateGameObject(const std::vector<std::string>& component_list);
	GameObject* CreateGameObject(json::JSON& _gameObject);
	// Find Game object by Name
	GameObject* FindGameObjectByName(std::string object_name);

	/// <summary>
	/// Finds all game objects with the specified component
	/// </summary>
	std::list<GameObject*> FindAllGameObjectsWithComponent(const std::string& comp_type);

private:
	/// <summary>
	/// Initialize the GameObjectManager
	/// </summary>
	/// <param name="document">The json document</param>
	void Initialize();

	/// <summary>
	/// Updates all game objects, as well this will destroy all game objects that have
	/// been tag for deletion after everything updates
	/// </summary>
	void Update();

public:
	static GameObjectManager& Instance()
	{
		if (_instance == nullptr)
		{
			_instance = new GameObjectManager();
		}
		return *_instance;
	}
private:
	static GameObjectManager* _instance;

	/// <summary>
	/// Destroy all game objects in the manager
	/// </summary>
	~GameObjectManager();

	GameObjectManager() = default;
	GameObjectManager(const GameObjectManager& other) = delete;
	GameObjectManager& operator= (const GameObjectManager& other) = delete;

	friend class GameEngine;
};

#endif //GAMEOBJECTMANAGER_H