///-------------------------------------------------------------------------------------------------
// file: TextureAsset.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef TEXTURE_ASSET
#define TEXTURE_ASSET

#include "Asset.h"

/// <summary>
/// Texture Asset
/// </summary>
class TextureAsset : public Asset
{
public:
	~TextureAsset() {};
};

#endif // !TEXTURE_ASSET
