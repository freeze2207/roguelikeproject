///-------------------------------------------------------------------------------------------------
// file: GameObject.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "GameObject.h"
#include "Component.h"

IMPLEMENT_DYNAMIC_CLASS(GameObject)

GameObject::~GameObject()
{
	for (auto component : components)
	{
		delete component;
	}
	components.clear();
}

void GameObject::Initialize()
{
	for (auto component : components)
	{
		component->Initialize();
	}
	for (auto component : components)
	{
		component->Start();
	}
}

void GameObject::Load(json::JSON& json_game_object)
{
	Object::Load(json_game_object);

	assert(json_game_object.hasKey("name"));
	name = json_game_object["name"].ToString();

	if (json_game_object.hasKey("position"))
	{
		json::JSON position_node = json_game_object["position"];
		if (position_node.hasKey("x"))
		{
			position.x = position_node["x"].ToFloat();
		}
		if (position_node.hasKey("y"))
		{
			position.y = position_node["y"].ToFloat();
		}
	}

	if (json_game_object.hasKey("components"))
	{
		auto json_components = json_game_object["components"];
		for (auto& json_component : json_components.ArrayRange())	// Loop for components
		{
			assert(json_component.hasKey("className"));
			std::string class_name = json_component["className"].ToString();

			Component* component = static_cast<Component*>(CreateObject(class_name.c_str()));
			component->Load(json_component);
			component->game_object = this;
			AddComponent(component);
		}
	}
}

void GameObject::AddComponents(const std::vector<std::string>& component_list)
{
	for (std::string comp : component_list)
	{
		Component* component = (Component*)CreateObject(comp.c_str());
		component->game_object = this;
		component->Initialize();
		AddComponent(component);
	}
}

void GameObject::Update()
{
	for (auto component : components)
	{
		component->Update();
	}

	for (auto component : components_to_remove)
	{
		components.remove(component);
		delete component;
	}
	components_to_remove.clear();
}

void GameObject::AddComponent(Component* component)
{
	components.push_back(component);
}

void GameObject::RemoveComponent(Component* component)
{
	components_to_remove.remove(component);
}

Component* const GameObject::FindComponentByType(const std::string& comp_type)
{
	for (auto component : components)
	{
		if (component->getDerivedClassName() == comp_type)
		{
			return component;
		}
	}
	return nullptr;
}
