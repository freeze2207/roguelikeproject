///-------------------------------------------------------------------------------------------------
// file: InputManager.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "Core.h"
#include "InputManager.h"
#include "RenderSystem.h"
#include <algorithm>

IMPLEMENT_SINGLETON(InputManager)

void InputManager::ToggleKeyPressedRepeated(bool toggle)
{
	RenderSystem::Instance().GetWindow().setKeyRepeatEnabled(toggle);
}

void InputManager::Initialize()
{
	std::cout << "InputManager initialized" << std::endl;
	RenderSystem::Instance().GetWindow().setKeyRepeatEnabled(false);
}


void InputManager::Update()
{
	sf::Event event;

	// Clear old button caches
	ClearMouseCaches();
	ClearJoystickCaches();
	ClearKeyCaches();

	while (RenderSystem::Instance().HasRenderWindow() && RenderSystem::Instance().GetWindow().pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			RenderSystem::Instance().CloseWindow();
			break;

			// Keyboard
		case sf::Event::KeyReleased:
			// Cache the events
			releasedKey.push_back(event.key.code);
			// Events -> Function Pointers
			if (keySubscribeReleased.find(event.key.code) != keySubscribeReleased.end())
			{
				RunSubscriptions(keySubscribeReleased[event.key.code]);
			}
			break;

		case sf::Event::KeyPressed:
			pressedKey.push_back(event.key.code);
			if (keySubscribePressed.find(event.key.code) != keySubscribePressed.end())
			{
				RunSubscriptions(keySubscribePressed[event.key.code]);
			}
			break;

			// Mouse
		case sf::Event::MouseButtonPressed:
			// Add button to cache
			pressedMouse.push_back(event.mouseButton.button);
			// Run subscribed functions
			if (mouseSubscribePressed.find(event.mouseButton.button) != mouseSubscribePressed.end()) {
				RunSubscriptions(mouseSubscribePressed[event.mouseButton.button]);
			}
			break;

		case sf::Event::MouseButtonReleased:
			// Add button to cache
			releasedMouse.push_back(event.mouseButton.button);
			// Run subscribed functions
			if (mouseSubscribeReleased.find(event.mouseButton.button) != mouseSubscribeReleased.end()) {
				RunSubscriptions(mouseSubscribeReleased[event.mouseButton.button]);
			}
			break;

			// Joystick
		case sf::Event::JoystickButtonPressed:
			// Add button to cache
			pressedJoystick->push_back(event.joystickButton.button);
			// Run subscribed functions
			for (int i = 0; i < 8; i++) {
				if (joystickSubscribePressed[i].find(event.joystickButton.button) != joystickSubscribePressed[i].end()) {
					RunSubscriptions(joystickSubscribePressed[i][event.joystickButton.button]);
				}
			}
			break;

		case sf::Event::JoystickButtonReleased:
			// Add button to cache
			releasedJoystick->push_back(event.joystickButton.button);
			// Run subscribed functions
			for (int i = 0; i < 8; i++) {
				if (joystickSubscribeReleased[i].find(event.joystickButton.button) != joystickSubscribeReleased[i].end()) {
					RunSubscriptions(joystickSubscribeReleased[i][event.joystickButton.button]);
				}
			}
			break;
		}
	}
}

long InputManager::FunctionPointerAddress(std::function<void()> func) {
	return *(long*)(char*)&func;
}

void InputManager::RunSubscriptions(std::list<SubFunc*> subFuncs)
{
	// Run all functions in list
	for (auto it : subFuncs) {
		// if function is null for some reason
		// just remove it from the list
		if (it->func != nullptr) {
			it->func();
		}
		else {
			subFuncs.remove(it);
		}
	}
}

#pragma region Mouse

void InputManager::ClearMouseCaches() {
	pressedMouse.clear();
	releasedMouse.clear();
}

bool InputManager::MouseButtonPressed(sf::Mouse::Button button)
{
	auto it = std::find(pressedMouse.begin(), pressedMouse.end(), button);
	return it != pressedMouse.end();
}

bool InputManager::MouseButtonReleased(sf::Mouse::Button button)
{
	// See if cached buttons released contains 
	auto it = std::find(releasedMouse.begin(), releasedMouse.end(), button);
	return it != releasedMouse.end();
}

bool InputManager::MouseButtonDown(sf::Mouse::Button button)
{
	return sf::Mouse::isButtonPressed(button);
}

sf::Vector2i InputManager::GetMousePosition()
{
	return sf::Mouse::getPosition();
}

void InputManager::SubscribeMouse(InputState state, sf::Mouse::Button button, std::function<void()> func_pointer, int priority)
{
	// Determine what map we are working with
	MouseMap* m;
	if (state == InputState::Pressed) {
		m = &mouseSubscribePressed;
	}
	else if (state == InputState::Released) {
		m = &mouseSubscribeReleased;
	}
	else { // state == InputState::Down
		m = &mouseSubscribeDown;
	}

	// Create SubFunc struct
	SubFunc* subFunc = new SubFunc(priority, func_pointer);

	// Check if map already has a list for the button
	if (m->find(button) != m->end()) {
		(*m)[button].push_back(subFunc);
	}
	else { // If not create new list
		m->emplace(button, std::list<SubFunc*> { subFunc });
	}

	// Sort the list
	(*m)[button].sort([](const SubFunc* func1, const SubFunc* func2)
		{
			return func1->priority > func2->priority;
		});
}

void InputManager::UnsubscribeMouse(InputState state, sf::Mouse::Button button, std::function<void()> func_pointer)
{
	// Determine what map we are working with
	MouseMap* m;
	if (state == InputState::Pressed) {
		m = &mouseSubscribePressed;
	}
	else if (state == InputState::Released) {
		m = &mouseSubscribeReleased;
	}
	else { // state == InputState::Down
		m = &mouseSubscribeDown;
	}

	// Check if button is registered
	if (m->find(button) == m->end()) {
		return;
	}

	// Remove pointer from list if in list
	for (auto it : (*m)[button]) {
		if (FunctionPointerAddress(it->func) == FunctionPointerAddress(func_pointer)) {
			(*m)[button].remove(it);
			break;
		}
	}

	// If list is now empty, remove button from map
	if ((*m)[button].size() == 0) {
		m->erase(button); // we know button exists
	}
}

#pragma endregion Mouse

#pragma region Joystick

void InputManager::ClearJoystickCaches() {
	for (int i = 0; i < 8; i++) {
		pressedJoystick[i].clear();
		releasedJoystick[i].clear();
	}
}

bool InputManager::JoystickButtonPressed(int id, unsigned int button)
{
	// See if cached buttons pressed contains button
	auto it = std::find(pressedJoystick[id].begin(), pressedJoystick[id].end(), button);
	return it != pressedJoystick[id].end();
}

bool InputManager::JoystickButtonReleased(int id, unsigned int button)
{
	// See if cached buttons released contains button
	auto it = std::find(releasedJoystick[id].begin(), releasedJoystick[id].end(), button);
	return it != releasedJoystick[id].end();
}

bool InputManager::JoystickButtonDown(int id, unsigned int button)
{
	return sf::Joystick::isButtonPressed(id, button);
}

void InputManager::SubscribeJoystick(int id, InputState state, unsigned int button, std::function<void()> func_pointer, int priority)
{
	// Determine what map we are working with
	JoystickMap* m;
	if (state == InputState::Pressed) {
		m = &(joystickSubscribePressed[id]);
	}
	else if (state == InputState::Released) {
		m = &(joystickSubscribeReleased[id]);
	}
	else { // state == InputState::Down
		m = &(joystickSubscribeDown[id]);
	}

	// Create SubFunc struct
	SubFunc* subFunc = new SubFunc(priority, func_pointer);

	// Check if map already has a list for the button
	if (m->find(button) != m->end()) {
		(*m)[button].push_back(subFunc);
	}
	else { // If not create new list
		m->emplace(button, std::list<SubFunc*> { subFunc });
	}

	// Sort the list
	(*m)[button].sort([](const SubFunc* func1, const SubFunc* func2)
		{
			return func1->priority > func2->priority;
		});
}

void InputManager::UnsubscribeJoystick(int id, InputState state, unsigned int button, std::function<void()> func_pointer)
{
	// Determine what map we are working with
	JoystickMap* m;
	if (state == InputState::Pressed) {
		m = &(joystickSubscribePressed[id]);
	}
	else if (state == InputState::Released) {
		m = &(joystickSubscribeReleased[id]);
	}
	else { // state == InputState::Down
		m = &(joystickSubscribeDown[id]);
	}

	// Check if button is registered
	if (m->find(button) == m->end()) {
		return;
	}

	// Remove pointer from list if in list
	for (auto it : (*m)[button]) {
		if (FunctionPointerAddress(it->func) == FunctionPointerAddress(func_pointer)) {
			(*m)[button].remove(it);
			break;
		}
	}

	// If list is now empty, remove button from map
	if ((*m)[button].size() == 0) {
		m->erase(button); // we know button exists
	}
}

float InputManager::JoystickAxisPosition(int id, sf::Joystick::Axis axis) {
	return sf::Joystick::getAxisPosition(id, axis);
}

int InputManager::JoysticksDetected() {
	int total = 0;
	for (int i = 0; i < 8; i++) {
		if (sf::Joystick::isConnected(i)) {
			total++;
		}
	}
	return total;
}

#pragma endregion Joystick

#pragma region Keyboard

void InputManager::ClearKeyCaches()
{
	pressedKey.clear();
	releasedKey.clear();
}

bool InputManager::KeyPressed(sf::Keyboard::Key key)
{
	auto it = std::find(pressedKey.begin(), pressedKey.end(), key);
	return it != pressedKey.end();
}

bool InputManager::KeyReleased(sf::Keyboard::Key key)
{
	auto it = std::find(releasedKey.begin(), releasedKey.end(), key);
	return it != releasedKey.end();
}

bool InputManager::KeyDown(sf::Keyboard::Key key)
{
	return sf::Keyboard::isKeyPressed(key);
}

void InputManager::SubscribeKey(InputState state, sf::Keyboard::Key key, std::function<void()> func_pointer, int priority)
{
	KeyMap* k;
	if (state == InputState::Pressed)
	{
		k = &keySubscribePressed;
	}
	else if (state == InputState::Released)
	{
		k = &keySubscribeReleased;
	}
	else // state == InputState::Down
	{
		k = &keySubscribeDown;
	}

	SubFunc* subFunc = new SubFunc(priority, func_pointer);

	if (k->find(key) != k->end())
	{
		(*k)[key].push_back(subFunc);
	}
	else
	{
		k->emplace(key, std::list<SubFunc*> { subFunc });
	}

	(*k)[key].sort([](const SubFunc* func1, const SubFunc* func2) {
		return func1->priority > func2->priority;
		});
}

void InputManager::UnsubscribeKey(InputState state, sf::Keyboard::Key key, std::function<void()> func_pointer)
{
	KeyMap* k;
	if (state == InputState::Pressed)
	{
		k = &keySubscribePressed;
	}
	else if (state == InputState::Released)
	{
		k = &keySubscribeReleased;
	}
	else // state == InputState::Down
	{
		k = &keySubscribeDown;
	}

	if (k->find(key) == k->end())
	{
		return;
	}

	for (auto it : (*k)[key])
	{
		if (FunctionPointerAddress(it->func) == FunctionPointerAddress(func_pointer))
		{
			(*k)[key].remove(it);
			break;
		}
	}

	if ((*k)[key].size() == 0)
	{
		k->erase(key); // we know key exists
	}
}

#pragma endregion Keyboard