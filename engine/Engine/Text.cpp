#include "Core.h"
#include "Text.h"
#include "RenderSystem.h"
#include "GameObject.h"

IMPLEMENT_DYNAMIC_CLASS(Text)

void Text::Load(json::JSON& json_component)
{
	Component::Load(json_component);

	// Should be part of asset manager, we should look up the asset by a GUID
	ASSERT(json_component.hasKey("uid"), "Json node missing");
	
	if (json_component.hasKey("layer"))
	{
		m_layer = json_component["layer"].ToInt();
	}
	if (json_component.hasKey("enable"))
	{
		m_enable = json_component["enable"].ToBool();
	}
	if (json_component.hasKey("fontSize"))
	{
		m_fontSize = json_component["fontSize"].ToInt();
	}
	if (json_component.hasKey("fontColor"))
	{
		sf::Color color(json_component["fontColor"]["r"].ToInt(), json_component["fontColor"]["g"].ToInt(), json_component["fontColor"]["b"].ToInt(), json_component["fontColor"]["a"].ToInt());
		m_fontColor = color;
	}
	if (json_component.hasKey("content"))
	{
		m_content = json_component["content"].ToString();
	}
	if (json_component.hasKey("origin"))
	{		
		m_origin = Align(json_component["origin"].ToInt());
	}
	
}

void Text::Initialize()
{
	
}

void Text::Start()
{
	// test rendering, get from asset for default font
	m_text.setPosition(game_object->GetPosition());
	m_font.loadFromFile("../Assets/Vogue.ttf");
	m_text.setFont(m_font);
	m_text.setString(m_content);
	m_text.setCharacterSize(m_fontSize);
	m_text.setFillColor(m_fontColor);
}

void Text::Render(float total_time)
{
	m_text.setPosition(game_object->GetPosition());
	m_font.loadFromFile("../Assets/Vogue.ttf");
	m_text.setFont(m_font);
	m_text.setString(m_content);
	m_text.setCharacterSize(m_fontSize);
	m_text.setFillColor(m_fontColor);
	RenderSystem::Instance().GetWindow().draw(m_text);
}

int Text::GetLayer()
{
	return m_layer;
}

sf::FloatRect Text::GetBounds()
{
	return m_text.getGlobalBounds();
}

bool Text::Contains(float _positionX, float _positionY)
{
	return m_enable ? m_text.getGlobalBounds().contains(sf::Vector2f(_positionX, _positionY)) : false;
}

void Text::SetFont(const sf::Font& _font)
{
	this->m_font = _font;
}

void Text::SetFontSize(const unsigned int& _fontSize)
{
	this->m_fontSize = _fontSize;
}

void Text::SetFontColor(const sf::Color& _color)
{
	this->m_fontColor = _color;
}

void Text::SetStyle(const sf::Text::Style& _style)
{
	this->m_fontStyle = _style;
}

void Text::SetText(const std::string& _text)
{
	this->m_content = _text;
}

void Text::SetOrigin(const Text::Align& _origin)
{
	this->m_origin = _origin;
}

void Text::SetLayer(const int& _layer)
{
	this->m_layer = _layer;
}

const sf::Font Text::GetFont()
{
	return this->m_font;
}

const unsigned int Text::GetFontSize()
{
	return this->m_fontSize;
}

const sf::Color Text::GetFontColor()
{
	return this->m_fontColor;
}

const sf::Text::Style Text::GetFontStyle()
{
	return this->m_fontStyle;
}

const std::string Text::GetText()
{
	return this->m_content;
}

const Text::Align Text::GetOrigin()
{
	return this->m_origin;
}
