///-------------------------------------------------------------------------------------------------
// file: GameObject.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <list>
#include "json.hpp"
#include "Object.h"

#include "SFML/System.hpp"

class Component;

/// <summary>
/// Defines the GameObject class
/// </summary>
class GameObject final : public Object
{
	DECLARE_DYNAMIC_DERIVED_CLASS(GameObject, Object)

private:
	std::string name = "";
	std::list<Component*> components;
	std::list<Component*> components_to_remove;

	// This should be in a transform, and all game objects should have a transform
	//Transform *transform;
	sf::Vector2f position;

public:
	/// <summary>
	/// Deletes all components in the game object
	/// </summary>
	~GameObject();

	/// <summary>
	/// 
	/// </summary>
	/// <param name="json_game_object"></param>
	void Load(json::JSON& json_game_object) override;

	/// <summary>
	/// 
	/// </summary>
	/// <param name="component_list"></param>
	void AddComponents(const std::vector<std::string>& component_list);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="component"></param>
	void AddComponent(Component* component);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="component"></param>
	void RemoveComponent(Component* component);

	template<class T>
	inline T* GetComponent() {
		T temp = T();
		std::string className = temp.getDerivedClassName();
		for (auto comp : components)
		{
			if (comp->getDerivedClassName() == className)
			{
				return (T*)comp;
			}
		}
		return nullptr;
	}

	template<class T>
	inline std::list<T*> GetComponents() {
		T temp = T();
		std::string className = temp.getDerivedClassName();
		std::list<T*> compList;
		for (auto comp : components)
		{
			if (comp->getDerivedClassName() == className)
			{
				compList.push_back((T*)comp);
			}
		}
		return compList;
	}

	Component* const FindComponentByType(const std::string& comp_type);

	//inline std::list<Component*>& GetComponents() { return components; }
	inline const std::string& GetName() { return name; }
	inline void SetPosition(const sf::Vector2f& value) { position = value; }
	inline const sf::Vector2f& GetPosition() { return position; }

private:
	/// <summary>
	/// Initialize the GameObject
	/// </summary>
	/// <param name="json_game_object"></param>
	void Initialize() override;

	/// <summary>
	/// 
	/// </summary>
	void Update();

	friend class GameObjectManager;
};

#endif //GAMEOBJECT_H

