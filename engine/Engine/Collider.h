#pragma once

#ifndef COLLIDER_H
#define COLLIDER_H

#include "ICollidable.h"
#include "Component.h"


class Collider : public Component, ICollidable
{
	DECLARE_ABSTRACT_DERIVED_CLASS(Collider, Component)
	Collider();
	~Collider();

private:
	std::list<std::function<void(ICollidable*)>> collisionEnterCallbacks;
	std::list<std::function<void(ICollidable*)>> collisionStayCallbacks;
	std::list<std::function<void(ICollidable*)>> collisionExitCallbacks;

	virtual const sf::Shape& GetShape() = 0;

protected:
	void OnCollisionEnter(ICollidable* collider) override;
	void OnCollisionStay(ICollidable* collider) override;
	void OnCollisionExit(ICollidable* collider) override;

public:
	void RegisterEnterCallback(std::function<void(ICollidable*)> func);
	void RegisterStayCallback(std::function<void(ICollidable*)> func);
	void RegisterExitCallback(std::function<void(ICollidable*)> func);

	friend class CollisionEngine;
};

#endif // !COLLIDER_H
