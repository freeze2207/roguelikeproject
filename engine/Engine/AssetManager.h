///-------------------------------------------------------------------------------------------------
// file: AssetManger.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

class Asset;

/// <summary>
/// Defines the AssetManager class
/// </summary>
class AssetManager final
{
private:
	std::list<Asset*> assets;

public:
	/// <summary>
	/// 
	/// </summary>
	/// <param name="asset"></param>
	void AddAsset(Asset* asset);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="asset"></param>
	void RemoveAsset(Asset* asset);

private:
	/// <summary>
	/// 
	/// </summary>
	void Initialize();

	/// <summary>
	/// 
	/// </summary>
	void Update();

	DECLARE_SINGLETON(AssetManager)

	friend class GameEngine;
};

#endif //ASSETMANAGER_H
