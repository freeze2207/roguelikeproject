#pragma once
#ifndef ISYSTEM_H
#define ISYSTEM_H

class IProjectEngine
{
public:
	virtual void Initialize() = 0;
	virtual void Update() = 0;
};

#endif