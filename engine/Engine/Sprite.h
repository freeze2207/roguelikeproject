///-------------------------------------------------------------------------------------------------
// file: Sprite.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef SPRITE_H
#define SPRITE_H

#include "Component.h"
#include "IRenderable.h"
#include "SFML/Graphics.hpp"

/// <summary>
/// Draws a sprite to the SFML window
/// </summary>
class Sprite final : public Component, IRenderable
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Sprite, Component)
public:
	friend class GameObjectManager;

	enum class Align
	{
		TopLeft = 0,
		Center,
		TopRight,
		BottomLeft,
		BottomRight
	};

	int GetLayer() override;
	void SetLayer(const int& _layer);
	void SetupStaticSprite(std::string& _textureFileName);
	void SetupStaticSprite(std::string& _textureFileName, sf::Vector2u _startPosition, int _offset, int _height);
	void SetupAnimatedSprite(std::string& _textureFileName, sf::Vector2u _startPosition, sf::Vector2u _endPosition, int _offset, int _height, float _interval = 0.5f);

	sf::FloatRect GetBounds() override;

	bool Contains(float _positionX, float _positionY) override;

	sf::Vector2f GetScale() const;
	void SetScale(const sf::Vector2f& _scale);
	void SetScale(const float& _scaleX, const float& _scaleY);
	void SetInterval(float _interval);

	const sf::Vector2f& GetOrigin();

	void SetOrigin(sf::Vector2f& _origin);

	bool GetStatus() override { return m_enable; }

	void Enable() { m_enable = true; }
	void Disable() { m_enable = false; }

	~Sprite() override {}
protected:

	/// <summary>
	/// Initialize the Sprite
	/// </summary>
	void Initialize() override;

	void Start() override;

	/// <summary>
	/// Load the sprite data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;
	
	/// <summary>
	/// Render the Sprite
	/// </summary>
	void Render(float total_time) override;
	void UpdateAnimation();

	void SetupStaticSprite(std::string& _textureFileName, bool isSliced);
	
	void SetupAnimatedSprite(std::string& _textureFileName);
	
	bool IsAnimation();
	void SetOrigin(Sprite::Align _align);

private:
	float			m_time = 0;							// Store old total Game time
	sf::Sprite		m_defaultSprite;					// Default sprite to render
	bool			m_enable;

	float			m_interval = 0.5f;					// animation change speed
	sf::Texture		texture;							// texture to load
	std::string		texture_filename;					// texture to load
	sf::Vector2u    textureSize;
	sf::Sprite		sprite;								// Sprite reference
	int				m_layer = 0;						// Layer info
	sf::Vector2f	m_scale = sf::Vector2f(1.0f, 1.0f);	// sprite scale
	bool			isAnimated = false;					// isAnimated flag
	int				m_offset = 0;							// width of each block that determinds the steps					
	sf::Vector2u	m_animationStartPosition;			// Start position of animation sprite sets
	sf::Vector2u	m_animationEndPosition;				// End position of animation sprite sets
	sf::IntRect		rectTarget = sf::IntRect (0, 0, 0, 0);	// Rect focus

	
};
#endif // !SPRITE_H
