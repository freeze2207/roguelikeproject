#pragma once
#ifndef OBJECTPOOL_H
#define OBJECTPOOL_H
#include "GameObject.h"

template <class T>
class ObjectPool
{
private:
	std::vector<T> objects;
	int poolSize = 0;

public:
	
	ObjectPool(int _size);
	

	T* GetPoolObject();
	void Retrieve(T* _object);
};

#endif // !OBJECTPOOL_H

