#pragma once
#ifndef MONSTER_H
#define MONSTER_H

#include "Component.h"

class Monster final : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Monster, Component)

private:
	std::string m_name = "";
	int m_health = 0;
	int m_moveSpeed = 0;
	int m_damage = 0;
	int m_attackRange = 0;

protected:
	~Monster() override;
	void Initialize() override;

	void Start() override;

	void Update() override;
	/// <summary>
	/// Load the data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;

public:
	std::string GetName() { return m_name; }
	int GetHealth() { return m_health; }
	int GetSpeed() { return m_moveSpeed; }
	int GetDamage() { return m_damage; }
	int GetRange() { return m_attackRange; }

};

#endif // !MONSTER_H
