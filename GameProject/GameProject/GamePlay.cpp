#include "GameCore.h"
#include "GamePlay.h"
#include "Sprite.h"
#include "Text.h"
#include "PlayerController.h"
#include "Character.h"
#include "EngineTime.h"


void GamePlay::Initialize()
{
	LoadMap();
	m_player = GameObjectManager::Instance().FindGameObjectByName("Player");
	m_playerCharacter = (Character*)GameObjectManager::Instance().FindGameObjectByName("Player")->FindComponentByType("Character");
	m_playerControl = (PlayerController*)GameObjectManager::Instance().FindGameObjectByName("Player")->FindComponentByType("PlayerController");
	m_menuBackground = (Sprite*)GameObjectManager::Instance().FindGameObjectByName("MenuBackground")->FindComponentByType("Sprite");
	m_menuText = (Text*)GameObjectManager::Instance().FindGameObjectByName("MenuText")->FindComponentByType("Text");
	m_continueButton = (Text*)GameObjectManager::Instance().FindGameObjectByName("ContinueText")->FindComponentByType("Text");
	m_startButton = (Text*)GameObjectManager::Instance().FindGameObjectByName("StartText")->FindComponentByType("Text");
	m_quitButton = (Text*)GameObjectManager::Instance().FindGameObjectByName("QuitText")->FindComponentByType("Text");
	m_winText = (Text*)GameObjectManager::Instance().FindGameObjectByName("WinText")->FindComponentByType("Text");
	m_loseText = (Text*)GameObjectManager::Instance().FindGameObjectByName("LoseText")->FindComponentByType("Text");
	m_roundText = (Text*)GameObjectManager::Instance().FindGameObjectByName("RoundText")->FindComponentByType("Text");
	m_HPText = (Text*)GameObjectManager::Instance().FindGameObjectByName("HPText")->FindComponentByType("Text");
	m_scoreText = (Text*)GameObjectManager::Instance().FindGameObjectByName("ScoreText")->FindComponentByType("Text");

	//pool = *(new ObjectPool<GameObject>(5));
}

void GamePlay::Update()
{
	m_block_list = GameObjectManager::Instance().FindAllGameObjectsWithComponent("Block");
	m_character_list = GameObjectManager::Instance().FindAllGameObjectsWithComponent("Character");

	// Pause the game
	if(isInGame && InputManager::Instance().KeyPressed(sf::Keyboard::Tilde))
	{
		isInGame = false;
		isPause = true;
	}


	// Game flow
	if (!isInGame)
	{
		if (isPause)
		{
			DisplayPauseMenu();
		}
		else
		{
			DisplayStartMenu();
		}
	}
	else
	{
		if (isPlayerWin || isPlayerDead)
		{
			GameOver();
		}
		else
		{
			InGame();
		}
	}
	
}

void GamePlay::LoadMap()
{
	// read from map
	std::ifstream game_map_stream("../Assets/RogueMap.json");
	std::string game_map_str((std::istreambuf_iterator<char>(game_map_stream)), std::istreambuf_iterator<char>());
	json::JSON game_map_document = json::JSON::Load(game_map_str);

	// Required fields for map
	ASSERT(game_map_document.hasKey("terrain_list"), "Default terrain_list node missing");
	ASSERT(game_map_document.hasKey("item_list"), "Default item_list node missing");

	json::JSON json_terrain_list = game_map_document["terrain_list"];
	json::JSON json_item_list = game_map_document["item_list"];
	

	for (int i = 0; i < json_terrain_list.length(); i++)
	{
		json::JSON newObj = json::JSON::Object();
		newObj["name"] = json_terrain_list[i]["m_name"];
		newObj["uid"] = "a4d059d2-3094-472f-afd2-62e2dc05ddaf";
		newObj["position"]["x"] = (float)(100 + (i % 10) * 64);
		newObj["position"]["y"] = (float)50 + floor(i * 0.1f) * 64;
		
		newObj["components"] = json::JSON::Array();

		// Add sprite component
		json::JSON newComponent = json::JSON::Object();
		newComponent["className"] = "Sprite";
		newComponent["uid"] = "4882b971-36c3-4a25-8d96-e2c95f9e3deb";
		newComponent["texture_filename"] = "../Assets/Textures/" + json_terrain_list[i]["m_textureName"].ToString();
		newComponent["layer"] = 0;
		newComponent["enable"] = true;
		newComponent["startPosition"]["x"] = (int)json_terrain_list[i]["m_rectPosition"]["x"].ToFloat();
		newComponent["startPosition"]["y"] = (int)json_terrain_list[i]["m_rectPosition"]["y"].ToFloat();
		newComponent["width"] = 64;
		newComponent["height"] = 64;
		
		// Add terrain component
		json::JSON terrainComp = json::JSON::Object();
		terrainComp["className"] = ConvertMapType(json_terrain_list[i]["m_type"].ToInt());
		terrainComp["uid"] = "4882b971-36c3-4a25-8d96-e2c95f9e3deb";

		newObj["components"].append(newComponent);
		newObj["components"].append(terrainComp);
		
		GameObjectManager::Instance().CreateGameObject(newObj);
	}

	for (int i = 0; i < json_item_list.length(); i++)
	{
		if (json_item_list[i]["m_name"].ToString() != "")
		{
			json::JSON newObj = json::JSON::Object();
			newObj["name"] = json_item_list[i]["m_name"];
			newObj["uid"] = "a4d059d2-3094-472f-afd2-62e2dc05ddaf";
			newObj["position"]["x"] = (float)(100 + (i % 10) * 64);
			newObj["position"]["y"] = (float)50 + floor(i * 0.1f) * 64;

			newObj["components"] = json::JSON::Array();
			
			// Add sprite component
			json::JSON newComponent = json::JSON::Object();
			newComponent["className"] = "Sprite";
			newComponent["uid"] = "4882b971-36c3-4a25-8d96-e2c95f9e3deb";
			newComponent["texture_filename"] = "../Assets/Textures/" + json_item_list[i]["m_textureName"].ToString();
			newComponent["layer"] = 1;
			newComponent["enable"] = true;
			newComponent["startPosition"]["x"] = (int)json_item_list[i]["m_rectPosition"]["x"].ToFloat();
			newComponent["startPosition"]["y"] = (int)json_item_list[i]["m_rectPosition"]["y"].ToFloat();
			newComponent["width"] = 64;
			newComponent["height"] = 64;

			// Add additional component
			json::JSON addOnComp = json::JSON::Object();
			addOnComp["className"] = ConvertMapType(json_item_list[i]["m_type"].ToInt());
			addOnComp["uid"] = "4882b971-36c3-4a25-8d96-e2c95f9e3deb";
			
			// Add box collider for item
			if (json_item_list[i]["m_type"].ToInt() == 0)
			{
				json::JSON boxComp = json::JSON::Object();
				boxComp["className"] = "BoxCollider";
				boxComp["uid"] = "4882b971-36c3-4a25-8d96-e2c95f9e3deb";
				boxComp["rectangle_size"]["x"] = 60.0f;
				boxComp["rectangle_size"]["y"] = 60.0f;
				newObj["components"].append(boxComp);
			}
			newObj["components"].append(newComponent);
			newObj["components"].append(addOnComp);

			GameObjectManager::Instance().CreateGameObject(newObj);
		}
	}

}

std::string GamePlay::ConvertMapType(int _type)
{
	switch (_type)
	{
	case 0:
		return "Item";
	case 1:
		return "Block";
	case 2:
		return "Terrain";
	default:
		return "";
	}
}

void GamePlay::DisplayStartMenu()
{
	// Disable player in game action control
	m_playerCharacter->SetMoveable(false);

	// Disable not used button
	m_continueButton->Disable();
	m_winText->Disable();
	m_loseText->Disable();

	// Bind Player mouse
	std::function<void()> func_ptr = std::bind(&GamePlay::MenuMouseAction, &GamePlay::Instance());
	InputManager::Instance().SubscribeMouse(InputManager::InputState::Released, sf::Mouse::Left, func_ptr);
}

void GamePlay::DisplayPauseMenu()
{
	// Disable player in game action control
	m_playerCharacter->SetMoveable(false);
	// Switch
	m_menuBackground->Enable();
	m_continueButton->Enable();
	m_quitButton->Enable();

}

void GamePlay::DisplayEndMenu()
{
	// TODO Disable player in game action control
	std::string endMessage = isPlayerWin ? "You Win!" : "You are Dead..";
	if (isPlayerWin)
	{
		m_winText->Enable();
	}
	else
	{
		m_loseText->Enable();
	}

	m_menuBackground->Enable();

	m_quitButton->Enable();
}

void GamePlay::MenuMouseAction()
{
	Text* continueTarget = isPause ? m_continueButton : m_startButton;

	if (RenderSystem::Instance().HasRenderWindow())
	{
		auto mouseLocationPx = sf::Mouse::getPosition(RenderSystem::Instance().GetWindow());
		m_mouseLocation = RenderSystem::Instance().GetWindow().mapPixelToCoords(mouseLocationPx);

		// click on target button (back to game)
		if (continueTarget->Contains(m_mouseLocation.x, m_mouseLocation.y))
		{
			m_menuBackground->Disable();
			m_menuText->Disable();
			continueTarget->Disable();
			m_quitButton->Disable();
			
			isInGame = true;
			isPause = false;
			return;
		}
		// click on quit
		else if (m_quitButton->Contains(m_mouseLocation.x, m_mouseLocation.y))
		{
			RenderSystem::Instance().CloseWindow();
			return;
		}
	}
	
}

void GamePlay::InGame()
{
	if (isPlayerTurn)
	{
		m_playerCharacter->SetMoveable(true);
		
		// Player move, 0 - 3, up, down, left, right
		if (InputManager::Instance().KeyPressed(sf::Keyboard::W) && !InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "up" << std::endl;
			m_playerCharacter->Move(0);
			isPlayerTurn = false;
		}
		if (InputManager::Instance().KeyPressed(sf::Keyboard::S) && !InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "down" << std::endl;
			m_playerCharacter->Move(1);
			isPlayerTurn = false;
		}
		if (InputManager::Instance().KeyPressed(sf::Keyboard::A) && !InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "left" << std::endl;
			m_playerCharacter->Move(2);
			isPlayerTurn = false;
		}
		if (InputManager::Instance().KeyPressed(sf::Keyboard::D) && !InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "right" << std::endl;
			m_playerCharacter->Move(3);
			isPlayerTurn = false;
		}
		// Player attack
		if (InputManager::Instance().KeyPressed(sf::Keyboard::W) && InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "Hit up" << std::endl;
			m_playerCharacter->Attack(0);
			isPlayerTurn = false;
		}
		if (InputManager::Instance().KeyPressed(sf::Keyboard::S) && InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "Hit down" << std::endl;
			m_playerCharacter->Attack(1);
			isPlayerTurn = false;
		}
		if (InputManager::Instance().KeyPressed(sf::Keyboard::A) && InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "Hit left" << std::endl;
			m_playerCharacter->Attack(2);
			isPlayerTurn = false;
		}
		if (InputManager::Instance().KeyPressed(sf::Keyboard::D) && InputManager::Instance().KeyDown(sf::Keyboard::LControl))
		{
			std::cout << "Hit right" << std::endl;
			m_playerCharacter->Attack(3);
			isPlayerTurn = false;
		}

		
	}
	else
	{
		// Monster logic
		// Attack player if in range
		// if not, move
		for (auto go : m_character_list)
		{
			Character* ch = go->GetComponent<Character>();
			if (ch->m_isAI && go->GetName() != "Lair")
			{
				std::list<sf::Vector2i> list = ch->GetPossibleAttackRange();
				auto it = std::find(list.begin(), list.end(), sf::Vector2i(m_player->GetPosition()));
				if (it != list.end())
				{
					ch->Attack(0);
					ch->Attack(1);
					ch->Attack(2);
					ch->Attack(3);
				}
				else
				{
					float time = EngineTime::Instance().GetTotalTime();
					ch->Move((int)time % 4);
				}
			}
		}

		//if (m_turnNumber > 3 && GameObjectManager::Instance().FindGameObjectByName("Lair") != nullptr)
		//{
		//	GameObject* newGo = pool.GetPoolObject();
		//	json::JSON obj = json::JSON::Object();
		//	obj.Load("{ \"components\": [{\"className\":\"Sprite\",\"uid\" : \"4882b971-36c3-4a25-8d96-e2c95f9e3deb\",\"texture_filename\" : \"../Assets/Textures/snk.png\",\"layer\" : 1,\"enable\" : true,\"startPosition\" : {\"x\":0,\"y\" : 0},\"endPosition\" : {\"x\":256,\"y\" : 192},\"width\" : 64,\"height\" : 64,\"interval\" : 0.2},{\"className\":\"BoxCollider\",\"uid\" : \"48845971-3ad3-4a25-8d96-e2f95f9e3deb\",\"rectangle_size\" : {\"x\":60,\"y\" : 60}},{\"className\":\"Character\",\"uid\" : \"7b7c030d-c1e1-41cf-8c0c-af036784f9fd\",\"isAI\" : true,\"moveable\" : false,\"name\" : \"SNK\",\"health\" : 2,\"moveSpeed\" : 1,\"damage\" : 1,\"range\" : 1},{\"className\":\"Block\",\"uid\" : \"48845971-3ad3-4a25-8d96-e2f95f9e3deb\"}] ,\"name\" : \"SNK\",\"position\" : {\"x\":100,\"y\" : 306},\"uid\" : \"a4d059d2-3094-472f-afd2-62112331ddaf\" }");
		//	/*newGo->Load(obj);
		//	
		//	newGo->SetPosition(GameObjectManager::Instance().FindGameObjectByName("Lair")->GetPosition());*/
		//}

		isPlayerTurn = true;
		m_turnNumber++;

		std::cout << m_playerCharacter->GetHealth() << std::endl;

		m_roundText->SetText("Round: " +  std::to_string(m_turnNumber));
		m_HPText->SetText("Hp: " + std::to_string( m_playerCharacter->GetHealth()));
		m_scoreText->SetText("Score: " + std::to_string( m_score));
	}

}

void GamePlay::GameOver()
{
	DisplayEndMenu();
}


