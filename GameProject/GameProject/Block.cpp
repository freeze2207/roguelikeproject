#include "GameCore.h"
#include "Block.h"

IMPLEMENT_DYNAMIC_CLASS(Block)

Block::~Block()
{
}

void Block::Initialize()
{
}

void Block::Start()
{
}

void Block::Update()
{
}

void Block::Load(json::JSON& json_component)
{
}
