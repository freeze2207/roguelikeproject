#pragma once
#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "IProjectEngine.h"
#include "ObjectPool.h"

class Text;
class Sprite;
class PlayerController;
class Character;


class GamePlay : public IProjectEngine
{

// Singleton implement
private:
	inline explicit GamePlay() = default;
	inline ~GamePlay() = default;
	inline explicit GamePlay(GamePlay const&) = delete;
	inline GamePlay& operator=(GamePlay const&) = delete;
	
public:
	void Initialize() override;
	void Update() override;


	inline static GamePlay& Instance()
	{
		static GamePlay instance;
		return instance;
	}

private:

	// game control references
	sf::Vector2f m_mouseLocation;
	Sprite* m_menuBackground = nullptr;
	Text* m_menuText = nullptr;
	Text* m_startButton = nullptr;
	Text* m_quitButton = nullptr;
	Text* m_continueButton = nullptr;
	Text* m_winText = nullptr;
	Text* m_loseText = nullptr;
	Text* m_roundText = nullptr;
	Text* m_HPText = nullptr;
	Text* m_scoreText = nullptr;

	GameObject* m_player = nullptr;
	PlayerController* m_playerControl = nullptr;
	Character* m_playerCharacter = nullptr;

	// In game logic flags
	bool isInGame = false;
	bool isPause = false;
	bool isPlayerTurn = true;
	
	int m_turnNumber = 0;

	void LoadMap();
	std::string ConvertMapType(int _type);

	void DisplayStartMenu();
	void DisplayPauseMenu();
	void DisplayEndMenu();
	void MenuMouseAction();

	void InGame();
	void GameOver();

public:
	int m_score = 0;
	bool isPlayerWin = false;
	bool isPlayerDead = false;
	std::list<GameObject*> m_block_list;
	std::list<GameObject*> m_character_list;
	//ObjectPool<GameObject> pool = ObjectPool<GameObject>(5);
};

#endif // !GAMEPLAY_H
