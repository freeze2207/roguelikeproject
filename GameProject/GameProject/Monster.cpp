#include "GameCore.h"
#include "Monster.h"

IMPLEMENT_DYNAMIC_CLASS(Monster)

Monster::~Monster()
{
}

void Monster::Initialize()
{
}

void Monster::Start()
{
}

void Monster::Update()
{
}

void Monster::Load(json::JSON& json_component)
{
}

