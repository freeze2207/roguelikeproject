///-------------------------------------------------------------------------------------------------
// file: gameRegisterClasses.cpp
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#include "GameCore.h"

// Game Include
#include "Zombie.h"
#include "Character.h"
#include "Item.h"
#include "Terrain.h"
#include "Block.h"

/// <summary>
/// 
/// </summary>
void GameRegisterClasses()
{
	REGISTER_DYNAMIC_CLASS(Zombie)
	REGISTER_DYNAMIC_CLASS(Character)
	REGISTER_DYNAMIC_CLASS(Item)
	REGISTER_DYNAMIC_CLASS(Terrain)
	REGISTER_DYNAMIC_CLASS(Block)

}