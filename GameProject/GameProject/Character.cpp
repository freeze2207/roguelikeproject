#include "GameCore.h"
#include "Character.h"
#include "GamePlay.h"
#include "Sprite.h"

IMPLEMENT_DYNAMIC_CLASS(Character)

Character::~Character()
{
}

void Character::Initialize()
{
}

void Character::Start()
{
	collider = game_object->GetComponent<BoxCollider>();
	pickup_func = std::bind(&Character::PickUpItem, *this, std::placeholders::_1);
	collider->RegisterExitCallback(pickup_func);

	check_func = std::bind(&Character::CheckItem, *this, std::placeholders::_1);
	collider->RegisterEnterCallback(check_func);
}

void Character::Update()
{
}

void Character::Load(json::JSON& json_component)
{
	if (json_component.hasKey("isAI"))
	{
		m_isAI = json_component["isAI"].ToBool();
	}
	if (json_component.hasKey("moveable"))
	{
		m_moveable = json_component["moveable"].ToBool();
	}
	if (json_component.hasKey("name"))
	{
		m_name = json_component["name"].ToString();
	}
	if (json_component.hasKey("health"))
	{
		m_health = json_component["health"].ToInt();
	}
	if (json_component.hasKey("moveSpeed"))
	{
		m_moveSpeed = json_component["moveSpeed"].ToInt();
	}
	if (json_component.hasKey("damage"))
	{
		m_damage = json_component["damage"].ToInt();
	}
	if (json_component.hasKey("range"))
	{
		m_attackRange = json_component["range"].ToInt();
	}
	
}

void Character::Dead()
{
	if (m_name == "Hero")
	{
		GamePlay::Instance().isPlayerDead = true;
	}
	else
	{
		GamePlay::Instance().m_score++;
	}
	GameObjectManager::Instance().RemoveGameObject(game_object);
}

std::list<sf::Vector2i> Character::GetPossibleAttackRange()
{
	std::list<sf::Vector2i> list;
	if (m_attackRange == 1)
	{
		list.push_back(sf::Vector2i(game_object->GetPosition()));
		list.push_back(sf::Vector2i(game_object->GetPosition().x + 64, game_object->GetPosition().y));
		list.push_back(sf::Vector2i(game_object->GetPosition().x - 64, game_object->GetPosition().y));
		list.push_back(sf::Vector2i(game_object->GetPosition().x , game_object->GetPosition().y + 64));
		list.push_back(sf::Vector2i(game_object->GetPosition().x , game_object->GetPosition().y - 64));

	}
	else
	{
		list.push_back(sf::Vector2i(game_object->GetPosition()));
		list.push_back(sf::Vector2i(game_object->GetPosition().x + 64, game_object->GetPosition().y));
		list.push_back(sf::Vector2i(game_object->GetPosition().x - 64, game_object->GetPosition().y));
		list.push_back(sf::Vector2i(game_object->GetPosition().x, game_object->GetPosition().y + 64));
		list.push_back(sf::Vector2i(game_object->GetPosition().x, game_object->GetPosition().y - 64));
		list.push_back(sf::Vector2i(game_object->GetPosition().x + 128, game_object->GetPosition().y));
		list.push_back(sf::Vector2i(game_object->GetPosition().x - 128, game_object->GetPosition().y));
		list.push_back(sf::Vector2i(game_object->GetPosition().x, game_object->GetPosition().y + 128));
		list.push_back(sf::Vector2i(game_object->GetPosition().x, game_object->GetPosition().y - 128));
	}
	return list;
}

void Character::Move(const int& _direction)
{
	sf::Vector2f movement = sf::Vector2f(0, 0);
	switch (_direction)
	{
	// up
	case 0:
		movement.y -= 64 * m_moveSpeed;
		break;
	// down
	case 1:
		movement.y += 64 * m_moveSpeed;
		break;
	// left
	case 2:
		movement.x -= 64 * m_moveSpeed;
		break;
	// right
	case 3:
		movement.x += 64 * m_moveSpeed;
		break;
	default:
		break;
	}
	// if there is no block in the new position or within bonds
	sf::Vector2i position = sf::Vector2i(game_object->GetPosition() + movement);
	std::list<sf::Vector2i> blocks;
	for (auto obj : GamePlay::Instance().m_block_list)
	{
		blocks.push_back(sf::Vector2i(obj->GetPosition()));
	}

	auto result_position = std::find(blocks.begin(),blocks.end(), position);

	if (bonds.contains(position) && result_position == blocks.end())
	{
		game_object->SetPosition(game_object->GetPosition() + movement);
	}
}

void Character::Attack(const int& _direction)
{
	// Hit location
	sf::Vector2f movement = sf::Vector2f(0, 0);
	switch (_direction)
	{
		// up
	case 0:
		movement.y -= 64 * m_attackRange;
		break;
		// down
	case 1:
		movement.y += 64 * m_attackRange;
		break;
		// left
	case 2:
		movement.x -= 64 * m_attackRange;
		break;
		// right
	case 3:
		movement.x += 64 * m_attackRange;
		break;
	default:
		break;
	}
	sf::Vector2i position = sf::Vector2i(game_object->GetPosition() + movement);

	std::list<sf::Vector2i> characters;
	for (auto obj : GamePlay::Instance().m_block_list)
	{
		characters.push_back(sf::Vector2i(obj->GetPosition()));
	}

	auto result_position = std::find(characters.begin(), characters.end(), position);

	if (result_position != characters.end())
	{
		int distance = std::distance(characters.begin(), result_position);
		auto it = GamePlay::Instance().m_block_list.begin();
		std::advance(it, distance);
		GameObject* target = *it;
		if (target->GetComponent<Character>() != nullptr)
		{
			target->GetComponent<Character>()->TakeDamge(m_damage);
		}
	}

}

void Character::PickUpItem(ICollidable* col)
{
	if (game_object->GetName() == "Player")
	{
		//std::cout << game_object->GetName() << std::endl;
		Collider* collider = (Collider*)col;
		//std::cout << collider->game_object->GetName() << std::endl;
		if (collider->game_object->FindComponentByType("Item") != nullptr)
		{
			if (game_object->GetName() == "Player" && collider->game_object->GetName() == "health_potion")
			{
				game_object->GetComponent<Character>()->AddHealth(5);
			}
			//std::cout << collider->game_object->GetName() << std::endl;
			//collider->game_object->GetComponent<Sprite>()->Disable();
			GameObjectManager::Instance().RemoveGameObject(collider->game_object);
		}
	}

}

void Character::CheckItem(ICollidable* col)
{
	Collider* collider = (Collider*)col;
	//std::cout << collider->game_object->GetName() << std::endl;
	if (collider->game_object->FindComponentByType("Item") != nullptr)
	{
		if (game_object->GetName() == "Player" && collider->game_object->GetName() == "key")
		{
			GamePlay::Instance().isPlayerWin = true;
			return;
		}

	}
}
