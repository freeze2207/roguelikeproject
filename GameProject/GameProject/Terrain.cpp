#include "GameCore.h"
#include "Terrain.h"
IMPLEMENT_DYNAMIC_CLASS(Terrain)

Terrain::~Terrain()
{
}

void Terrain::Initialize()
{
}

void Terrain::Start()
{
}

void Terrain::Update()
{
}

void Terrain::Load(json::JSON& json_component)
{
}
