#pragma once
#ifndef ITEM_H
#define ITEM_H

#include "Component.h"
class Item final : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Item, Component)

private:


protected:
	~Item() override;
	void Initialize() override;

	void Start() override;

	void Update() override;
	/// <summary>
	/// Load the data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;

};

#endif // !ITEM_H
