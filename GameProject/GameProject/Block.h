#pragma once
#ifndef BLOCK_H
#define BLOCK_H

#include "Component.h"
#include "BoxCollider.h"

class Block final : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Block, Component)

private:
	BoxCollider* collider = nullptr;

protected:
	~Block() override;
	void Initialize() override;

	void Start() override;
	void Update() override;
	/// <summary>
	/// Load the data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;

public:
	std::function<void(ICollidable*)> func;
};

#endif // !BLOCK_H
