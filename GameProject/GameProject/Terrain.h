#pragma once
#ifndef TERRAIN_H
#define TERRAIN_H
#include "Component.h"

class Terrain final: public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Terrain, Component)
private:
	bool canPass = false;
protected:
	~Terrain() override;

	void Initialize() override;

	void Start() override;

	void Update() override;
	/// <summary>
	/// Load the data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;
public:

	bool GetPassStatus() { return canPass; }
	void SetPass(bool _bool) { this->canPass = _bool; }
};

#endif // !TERRAIN_H
