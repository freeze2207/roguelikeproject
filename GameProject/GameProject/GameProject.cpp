///-------------------------------------------------------------------------------------------------
// file: GameOBject.cpp
//
// author: William Barry
// date: 2020
//
// summary:	Entry point for the program
///-------------------------------------------------------------------------------------------------
#include "GameCore.h"
#include "GamePlay.h"
extern void GameRegisterClasses();

int main()
{
	GameRegisterClasses();

	// You can create a game engine singleton and have the game engine 
	// update your engine. Think of this as a game manager? or a class
	// that can do stuff for your game specifically.
	GameEngine::Instance().SetProjectEngine(&GamePlay::Instance());
	GameEngine::Instance().Initialize();
	GamePlay::Instance().Initialize();
	GameEngine::Instance().Update();
}
