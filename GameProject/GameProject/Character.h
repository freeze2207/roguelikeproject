#pragma once
#ifndef CHARACTER_H
#define CHARACTER_H

#include "Component.h"
#include "BoxCollider.h"

class Character final : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Character, Component)

private:
	std::string m_name = "";
	int m_health = 0;
	int m_moveSpeed = 0;
	int m_damage = 0;
	int m_attackRange = 0;
	bool m_moveable = false;
	bool isCollided = false;
	sf::IntRect bonds = sf::IntRect(100, 50, 640, 640);

	BoxCollider* collider = nullptr;

protected:
	void Initialize() override;

	void Update() override;
	/// <summary>
	/// Load the data
	/// </summary>
	/// <param name="json_component"></param>
	void Load(json::JSON& json_component) override;
	
	void Dead();

public:
	bool m_isAI = false;

	~Character() override;
	void Start() override;

	std::string GetName() { return m_name; }
	int GetHealth() { return m_health; }
	int GetSpeed() { return m_moveSpeed; }
	int GetDamage() { return m_damage; }
	int GetRange() { return m_attackRange; }
	std::list<sf::Vector2i> GetPossibleAttackRange();

	void AddHealth(const int& _point) { m_health += _point; if (m_health > 10) m_health = 10; }
	void SetMoveable(bool _bool) { m_moveable = _bool; }

	void Move(const int& _direction);

	void Attack(const int& _direction);

	void TakeDamge(const int _damage) { m_health -= _damage; if (m_health <= 0) Dead(); }

	void PickUpItem(ICollidable* col);

	void CheckItem(ICollidable* col);

	std::function<void(ICollidable*)> pickup_func;

	std::function<void(ICollidable*)> check_func;
};

#endif // !CHARACTER_H
