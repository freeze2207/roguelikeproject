///-------------------------------------------------------------------------------------------------
// file: Zombie.h
//
// author: William Barry
// date: 2020
//
// summary:	
///-------------------------------------------------------------------------------------------------
#pragma once
#ifndef ZOMBIE_H
#define ZOMBIE_H

#include "Component.h"

/// <summary>
/// 
/// </summary>
class Zombie : public Component
{
	DECLARE_DYNAMIC_DERIVED_CLASS(Zombie, Component)

public:
	~Zombie() override {}
};

#endif // !ZOMBIE_H

